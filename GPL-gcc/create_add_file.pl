#! /usr/bin/perl -w

use strict;
use warnings;

my @kategorie;
my @input_files;
my $ARCH = uc($ENV{ARCH});
my $FILE_ARCH = "unknown";
my $TOOL_ARCH = "unknown";
my $filesystem = "../archiv/tmp-" . $ENV{KERNEL_LAYOUT} . "-gcc_x86_64/";
my $filesystem_esc = "\.\.\/archiv\/tmp-" . $ENV{KERNEL_LAYOUT} . "-gcc_x86_64\/";
my $debug = 0;

if(defined($ARCH)) {
    if($ARCH eq "MIPSEL") {
        $FILE_ARCH = "MIPS";
        $TOOL_ARCH = "mipsel";
    }
    if($ARCH eq "MIPS") {
        $FILE_ARCH = "MIPS";
        $TOOL_ARCH = "mips";
    }
    if($ARCH eq "ARM") {
        $FILE_ARCH = "ARM";
        $TOOL_ARCH = "mipsel";
    }
    if($ARCH eq "ARMEB") {
        $FILE_ARCH = "ARM";
        $TOOL_ARCH = "armeb";
    }
    if($ARCH eq "ARMEB") {
        $FILE_ARCH = "ARM";
        $TOOL_ARCH = "armeb";
    }
    if($ARCH eq "X86") {
        $FILE_ARCH = "Intel 80386";
        $TOOL_ARCH = "i686";
    }
}


##########################################################################################
#
##########################################################################################
push @kategorie, ({
    "tag" => "BASIS",
    "order" => "1",
    "exclude_dirs" => [
        "rootfs/usr/bin/ncurses5-config",
        "rootfs/sbin/chcpu",
        "rootfs/sbin/ldconfig",
        "rootfs/sbin/findfs",
        "rootfs/sbin/fsfreeze",
        "rootfs/sbin/fstrim",
        "rootfs/sbin/blockdev",
        "rootfs/sbin/fdisk",
        "rootfs/sbin/cfdisk",
        "rootfs/sbin/losetup",
        "rootfs/sbin/sfdisk",
        "rootfs/sbin/wipefs",
        "rootfs/sbin/swaplabel",
        "rootfs/sbin/hwclock",
        "rootfs/sbin/ctrlaltdel",
        "rootfs/sbin/fsck.minix",
        "rootfs/sbin/mkfs.bfs",
        "rootfs/sbin/mkfs.minix",
        "rootfs/sbin/mkfs",
        "rootfs/usr/sbin/mkfs.ext4dev",
        "rootfs/usr/sbin/mkfs.ext4",
        "rootfs/usr/sbin/mkfs.ext3",
        "rootfs/usr/sbin/mkfs.ext2",
        "rootfs/usr/sbin/mke2fs",
        "rootfs/usr/bin/col",
        "rootfs/usr/bin/colcrt",
        "rootfs/usr/bin/column",
        "rootfs/usr/bin/cytune",
        "rootfs/usr/bin/tailf",
        "rootfs/usr/bin/getopt",
        "rootfs/usr/bin/ldd",
        "rootfs/usr/bin/strace",
        "rootfs/lib/libasan*",
        "rootfs/lib/libubsan*",
        "rootfs/lib/libthread_db*.so*",
        "rootfs/lib/libthread_db-*.so",
        "rootfs/bin/findmnt",
        "rootfs/bin/more",
        "rootfs/bin/lsblk",
        "rootfs/bin/lsof",
        "rootfs/bin/mountpoint",
        "rootfs/bin/wdctl",
        "rootfs/sbin/swapon",
        "rootfs/sbin/swapoff",
        "rootfs/sbin/swaplabel",
        "rootfs/sbin/mkswap",
        "rootfs/bin/mount",
        "rootfs/lib/libatomic.so.*",
        "rootfs/lib/libfdisk.so.*",
        "rootfs/lib/libmount.so.*",
        "rootfs/lib/libsmartcols.so.*",
        "rootfs/lib/libtirpc.so.*",         ## kein libtirpc. CVE-2017-8779 bzw. JZ-35127 beachten
        "rootfs/usr/lib/libtirpc.so*",      ## kein libtirpc. CVE-2017-8779 bzw. JZ-35127 beachten
        "rootfs/lib/libuargp.*",
        "rootfs/lib/libutil.*",
        "rootfs/sbin/fsck",
        "rootfs/sbin/mount.nfs.*",
        "rootfs/sbin/osd_login",
        "rootfs/sbin/umount.nfs.*",
        "rootfs/usr/lib/libcom_err.so.*",
        "rootfs/usr/lib/libe2p.so.*",
        "rootfs/usr/lib/libext2fs.so.*",
        "rootfs/usr/sbin/e2fsck",
        "rootfs/usr/sbin/fsck.*",
        "rootfs/etc/mke2fs.conf",
        "rootfs/usr/lib/libxml2.*so.*",      ## kein libxml2 (2.9.4). CVE-2017-8872 bzw. JZ-35395 beachten
        "rootfs/usr/bin/bspatch",
        "rootfs/usr/bin/bsdiff",
        "rootfs/usr/bin/cal",
        "rootfs/usr/bin/captest",
        "rootfs/usr/bin/cgroupfs-mount",
        "rootfs/usr/bin/cgroupfs-umount",
        "rootfs/usr/bin/chattr",
        "rootfs/usr/bin/fgconsole",
        "rootfs/usr/bin/filecap",
        "rootfs/usr/bin/flock",
        "rootfs/usr/bin/fusermount",
        "rootfs/usr/bin/gcore",
        "rootfs/usr/bin/gdb",
        "rootfs/usr/bin/gdbm_dump",
        "rootfs/usr/bin/gdbm_load",
        "rootfs/usr/bin/gdbmtool",
        "rootfs/usr/bin/gdbserver",
        "rootfs/usr/bin/getconf",
        "rootfs/usr/bin/gettext",
        "rootfs/usr/bin/gettext.sh",
        "rootfs/usr/bin/groups",
        "rootfs/usr/bin/hexdump",
        "rootfs/usr/bin/iconv",
        "rootfs/usr/bin/ipcmk",
        "rootfs/usr/bin/ipcrm",
        "rootfs/usr/bin/ipcs",
        "rootfs/usr/bin/isosize",
        "rootfs/usr/bin/linux32",
        "rootfs/usr/bin/linux64",
        "rootfs/usr/bin/logger",
        "rootfs/usr/bin/look",
        "rootfs/usr/bin/lsattr",
        "rootfs/usr/bin/lscpu",
        "rootfs/usr/bin/lsipc",
        "rootfs/usr/bin/lslocks",
        "rootfs/usr/bin/lslogins",
        "rootfs/usr/bin/lsns",
        "rootfs/usr/bin/lsof",
        "rootfs/usr/bin/mcookie",
        "rootfs/usr/bin/namei",
        "rootfs/usr/bin/netcap",
        "rootfs/usr/bin/ngettext",
        "rootfs/usr/bin/pg",
        "rootfs/usr/bin/pmap",
        "rootfs/usr/bin/prlimit",
        "rootfs/usr/bin/pscap",
        "rootfs/usr/bin/pstree",
        "rootfs/usr/bin/pwdx",
        "rootfs/usr/bin/rev",
        "rootfs/usr/bin/script",
        "rootfs/usr/bin/scriptreplay",
        "rootfs/usr/bin/setarch",
        "rootfs/usr/bin/setsid",
        "rootfs/usr/bin/sha3sum",
        "rootfs/usr/bin/smemcap",
        "rootfs/usr/bin/uname26",
        "rootfs/usr/bin/unionfs",
        "rootfs/usr/bin/unionfsctl",
        "rootfs/usr/bin/unxz",
        "rootfs/usr/bin/uuidgen",
        "rootfs/usr/bin/whereis",
        "rootfs/usr/bin/whois",
        "rootfs/usr/bin/xz",
        "rootfs/usr/bin/xzcat",
        "rootfs/usr/lib32",
        "rootfs/usr/lib/libbfd.*.so",
        "rootfs/usr/lib/libcap-ng.*so.*",
        "rootfs/usr/lib/libfdisk.so",
        "rootfs/usr/lib/libfuse.*so.*",
        "rootfs/usr/lib/libgdbm.*so.*",
        "rootfs/usr/lib/libgomp.*.so.*",
        "rootfs/usr/lib/libjpeg.*.so.*",
        "rootfs/usr/lib/libmount.so",
        "rootfs/usr/lib/libncurses.*so.*",
        "rootfs/usr/lib/libopcodes.*so",
        "rootfs/usr/lib/libsmartcols.so",
        "rootfs/usr/lib/libss.*so.*",
        "rootfs/usr/lib/libstdc++.*so.*",
        "rootfs/usr/lib/liburcu-bp.so.*",
        "rootfs/usr/lib/liburcu-cds.so.*",
        "rootfs/usr/lib/liburcu-common.so.*",
        "rootfs/usr/lib/liburcu-mb.so.*",
        "rootfs/usr/lib/liburcu-qsbr.so.*",
        "rootfs/usr/lib/liburcu-signal.so.*",
        "rootfs/usr/lib/liburcu.so.*",
        "rootfs/usr/lib/terminfo",
        "rootfs/usr/lib/libbz2.*so.*",
        "rootfs/bin/bash",
        "rootfs/usr/lib/libreadline.*so.*",
        "rootfs/usr/lib/libhistory.*so.*"
        ],
    "include_dirs" => [
        "rootfs/bin",
        "rootfs/usr/bin",
        "rootfs/sbin",
        "rootfs/lib",
        "rootfs/usr/lib",
        "rootfs/dev/log",
        "rootfs/lib/ld-uClibc.*.so",
        "rootfs/lib/ld-uClibc.so.0",
        "rootfs/usr/bin/[",
        "rootfs/usr/bin/[[",
        "rootfs/usr/bin/arping",
        "rootfs/usr/bin/awk",
        "rootfs/usr/bin/basename",
        "rootfs/usr/bin/bunzip2",
        "rootfs/usr/bin/bzcat",
        "rootfs/usr/bin/bzip2",
        "rootfs/usr/bin/cc",
        "rootfs/usr/bin/cmp",
        "rootfs/usr/bin/cut",
        "rootfs/usr/bin/dirname",
        "rootfs/usr/bin/du",
        "rootfs/usr/bin/env",
        "rootfs/usr/bin/ether-wake",
        "rootfs/usr/bin/expr",
        "rootfs/usr/bin/find",
        "rootfs/usr/bin/free",
        "rootfs/usr/bin/ftpget",
        "rootfs/usr/bin/ftpput",
        "rootfs/usr/bin/id",
        "rootfs/usr/bin/killall",
        "rootfs/usr/bin/killall5",
        "rootfs/usr/bin/less",
        "rootfs/usr/bin/logname",
        "rootfs/usr/bin/md5sum",
        "rootfs/usr/bin/mkfifo",
        "rootfs/usr/bin/nc",
        "rootfs/usr/bin/nohup",
        "rootfs/usr/bin/nslookup",
        "rootfs/usr/bin/passwd",
        "rootfs/usr/bin/printf",
        "rootfs/usr/bin/readlink",
        "rootfs/usr/bin/realpath",
        "rootfs/usr/bin/renice",
        "rootfs/usr/bin/reset",
        "rootfs/usr/bin/seq",
        "rootfs/usr/bin/sort",
        "rootfs/usr/bin/strace",
        "rootfs/usr/bin/tail",
        "rootfs/usr/bin/tee",
        "rootfs/usr/bin/test",
        "rootfs/usr/bin/tftp",
        "rootfs/usr/bin/time",
        "rootfs/usr/bin/top",
        "rootfs/usr/bin/tr",
        "rootfs/usr/bin/traceroute",
        "rootfs/usr/bin/traceroute6",
        "rootfs/usr/bin/tty",
        "rootfs/usr/bin/uniq",
        "rootfs/usr/bin/unzip",
        "rootfs/usr/bin/uptime",
        "rootfs/usr/bin/wc",
        "rootfs/usr/bin/wget",
        "rootfs/usr/bin/which",
        "rootfs/usr/bin/xargs",
        "rootfs/usr/lib/libc.so",
        "rootfs/usr/lib/libcrypt.so",
        "rootfs/usr/lib/libdl.so.*",
        "rootfs/usr/lib/libgcc_s.so",
        "rootfs/usr/lib/libm.so",
        "rootfs/lib/libpthread.*so.*",
        "rootfs/usr/lib/librt.so",
        "rootfs/usr/sbin/brctl",
        "rootfs/usr/sbin/chroot",
        "rootfs/usr/sbin/inetd",
        "rootfs/usr/sbin/telnetd",
        "rootfs/usr/lib/libintl.so",
        "rootfs/usr/bin/ldd",
        "rootfs/usr/lib/libcap.*so.*",
        "rootfs/usr/lib/libiconv.so.*",
        "rootfs/usr/bin/taskset",
        "rootfs/usr/sbin/sendarp",
        "rootfs/usr/lib/gconv",
        "rootfs/usr/lib/libbsd.*so.*",
    ]        
});

push @kategorie, ({
    "tag" => "DEV_GCC",
    "order" => "10",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/lib/libgettextpo.so",
        "rootfs/usr/lib/libmudflapth.so.0",
        "rootfs/usr/lib/libgettextpo.so.0",
        "rootfs/usr/lib/libpthread_pic.a",
        "rootfs/usr/lib/libm_pic.a",
        "rootfs/usr/lib/libthread_db_pic.a",
        "rootfs/usr/share/terminfo/v/vt200",
        "rootfs/usr/lib/terminfo",
        "rootfs/usr/include/ncurses.h",
        "rootfs/usr/lib/libcurses.a",
        "rootfs/usr/lib/libgomp.so",
        "rootfs/usr/lib/libmudflapth.so",
        "rootfs/usr/lib/libmpfr.so",
        "rootfs/usr/lib/libmpfr.so.1",
        "rootfs/usr/lib/libgettextsrc.so",
        "rootfs/usr/lib/libmudflap.so.0",
        "rootfs/usr/lib/libgmp.so",
        "rootfs/usr/lib/libgmp.so.3",
        "rootfs/usr/lib/libmudflap.so",
        "rootfs/usr/lib/libgettextlib.so",
        "rootfs/usr/lib/libgomp.so.1",
        "rootfs/usr/lib/librt_pic.a",
        "rootfs/usr/lib/libcrypt_pic.a",
        "rootfs/usr/lib/libdl_pic.a",
        "rootfs/usr/lib/libc_pic.a",
        "rootfs/usr/lib/libtermcap.a",
        "rootfs/usr/share/locale",
        "rootfs/usr/mips-unknown-linux-uclibc",
        "rootfs/usr/lib/libmudflapth.so.0.0.0",
        "rootfs/usr/lib/libgettextpo.a",
        "rootfs/usr/lib/crt1.o",
        "rootfs/usr/lib/libthread_db.a",
        "rootfs/usr/lib/uclibc_nonshared.a",
        "rootfs/usr/lib/libstdc[+]*.a",
        "rootfs/usr/lib/gcc",
        "rootfs/usr/lib/libmudflapth.a",
        "rootfs/usr/lib/libm.a",
        "rootfs/usr/lib/libform.a",
        "rootfs/usr/lib/libgettextpo.la",
        "rootfs/usr/lib/libgettextsrc.a",
        "rootfs/usr/lib/libbfd.a",
        "rootfs/usr/lib/libmpfr.a",
        "rootfs/usr/lib/libgomp.a",
        "rootfs/usr/lib/libintl.la",
        "rootfs/usr/lib/libmpfr.la",
        "rootfs/usr/lib/crti.o",
        "rootfs/usr/lib/libncurses.a",
        "rootfs/usr/lib/libgomp.spec",
        "rootfs/usr/lib/libfl.a",
        "rootfs/usr/lib/libmenu.a",
        "rootfs/usr/lib/libltdl.a",
        "rootfs/usr/lib/libsupc[+]*.a",
        "rootfs/usr/lib/libgmp.la",
        "rootfs/usr/lib/liby.a",
        "rootfs/usr/lib/libpanel.a",
        "rootfs/usr/lib/libiberty.a",
        "rootfs/usr/lib/Scrt1.o",
        "rootfs/usr/lib/libmudflap.a",
        "rootfs/usr/lib/libopcodes.a",
        "rootfs/usr/lib/libfl_pic.a",
        "rootfs/usr/lib/libgettextlib.a",
        "rootfs/usr/lib/libintl.a",
        "rootfs/usr/lib/libc.a",
        "rootfs/usr/lib/crtn.o",
        "rootfs/usr/lib/libltdl.la",
        "rootfs/usr/lib/libdl.a",
        "rootfs/usr/lib/ldscripts",
        "rootfs/usr/lib/libmudflap.so.0.0.0",
        "rootfs/usr/lib/libgettextlib-0.16.1.so",
        "rootfs/usr/lib/libgettextsrc-0.16.1.so",
        "rootfs/usr/lib/libmpfr.so.1.2.0",
        "rootfs/usr/lib/libgettextpo.so.0.3.0",
        "rootfs/usr/lib/libgomp.so.1.0.0",
        "rootfs/usr/lib/libgettextlib.la",
        "rootfs/usr/lib/librt.a",
        "rootfs/usr/lib/libcrypt.a",
        "rootfs/usr/lib/libgmp.a",
        "rootfs/usr/lib/libgettextsrc.la",
        "rootfs/usr/lib/libgmp.so.3.4.4",
        "rootfs/usr/bin/addr2line",
        "rootfs/usr/bin/readelf",
        "rootfs/usr/bin/mips-unknown-linux-uclibc-c[+]*",
        "rootfs/usr/bin/cpp",
        "rootfs/usr/bin/gawk",
        "rootfs/usr/bin/gprof",
        "rootfs/usr/bin/gcc",
        "rootfs/usr/lib/libpthread.a",
        "rootfs/usr/bin/size",
        "rootfs/usr/bin/as",
        "rootfs/usr/bin/gcov",
        "rootfs/usr/bin/ar",
        "rootfs/usr/bin/ld",
        "rootfs/usr/bin/mips-unknown-linux-uclibc-g.*",
        "rootfs/usr/bin/objcopy",
        "rootfs/usr/bin/ranlib",
        "rootfs/usr/bin/gccbug",
        "rootfs/usr/bin/iconv",
        "rootfs/usr/bin/c[+]*",
        "rootfs/usr/bin/g[+]*",
        "rootfs/usr/bin/c.*filt",
        "rootfs/usr/bin/strings",
        "rootfs/usr/bin/objdump",
        "rootfs/usr/bin/strip",
        "rootfs/usr/bin/mips-unknown-linux-uclibc-gcc-.*",
        "rootfs/usr/bin/nm",
        "rootfs/usr/libexec",
        "rootfs/usr/share/locale",
        "rootfs/usr/include",
        "rootfs/etc/ld.so.cache",
        "rootfs/sbin/ldconfig"
        ]
});

push @kategorie, ({
    "tag" => "DROP",
    "order" => "99",
    "exclude_dirs" => [
        ${filesystem} . "/rootfs/tmp/ldconfig"
        ],
    "include_dirs" => [
        ]
});

push @kategorie, ({
    "tag" => "CXX",
    "order" => "9",
    "exclude_dirs" => [
        "rootfs/usr/lib/libstdc.*so.*-gdb.py",   ### lib stdc++
        ],
    "include_dirs" => [
        "rootfs/usr/lib/libstdc.*so.*",   ### lib stdc++
        ]
});

push @kategorie, ({
    "tag" => "STRACE",
    "order" => "9",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/bin/strace"
        ]
});

push @kategorie, ({
    "tag" => "GDB",
    "order" => "9",
    "exclude_dirs" => [
        "rootfs/usr/bin/ncurses5-config"
        ],
    "include_dirs" => [
        "rootfs/usr/lib/libncurses.so",
        "rootfs/usr/bin/gdbserver",
        "rootfs/usr/bin/gdb",
        "rootfs/lib/libthread_db-*.so",
        "rootfs/lib/libthread_db*.so*",
        ]
});

push @kategorie, ({
    "tag" => "USB_HOST",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/blkid",
        "rootfs/usr/sbin/fsck",
        "rootfs/usr/lib/libuuid.so",
        "rootfs/usr/lib/libblkid.so",
        ]
});

push @kategorie, ({
    "tag" => "EXT4",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/fsck.ext4",
        "rootfs/usr/sbin/fsck.ext4dev"
        ]
});

push @kategorie, ({
    "tag" => "EXT3",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/fsck.ext3"
    ]
});

push @kategorie, ({
    "tag" => "MKFS",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/mkfs",
        "rootfs/usr/sbin/mkfs.ext4dev",
        "rootfs/usr/sbin/mkfs.ext4",
        "rootfs/usr/sbin/mkfs.ext3",
        "rootfs/usr/sbin/mkfs.ext2",
        "rootfs/etc/mke2fs.conf",
        "rootfs/usr/sbin/mke2fs"
    ]
});

push @kategorie, ({
    "tag" => "EXT2",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/e2fsck",
        "rootfs/usr/bin/chattr",
        "rootfs/usr/lib/libe2p.so",
        "rootfs/usr/sbin/fsck.ext2",
        "rootfs/usr/lib/libext2fs.so",
        "rootfs/usr/lib/libcom_err.so"
    ]
});

push @kategorie, ({
    "tag" => "UBI",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/ubiattach",
        "rootfs/usr/sbin/ubiblock",
        "rootfs/usr/sbin/ubicrc32",
        "rootfs/usr/sbin/ubidetach",
        "rootfs/usr/sbin/ubiformat",
        "rootfs/usr/sbin/ubimkvol",
        "rootfs/usr/sbin/ubinfo",
        "rootfs/usr/sbin/ubinize",
        "rootfs/usr/sbin/ubirename",
        "rootfs/usr/sbin/ubirmvol",
        "rootfs/usr/sbin/ubirsvol",
        "rootfs/usr/sbin/ubiupdatevol",
        "rootfs/usr/sbin/flash_erase",
    ]
});

push @kategorie, ({
    "tag" => "EXT_EXTRA",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        ]
});

push @kategorie, ({
    "tag" => "DEV_TOOLS",
    "order" => "10",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/bin/gettext",
        "rootfs/usr/lib/awk/grcat",
        "rootfs/usr/lib/awk/pwcat",
        "rootfs/usr/lib/libltdl.so",
        "rootfs/usr/bin/patch",
        "rootfs/usr/bin/make",
        "rootfs/usr/bin/bison",
        "rootfs/usr/bin/pgawk",
        "rootfs/usr/bin/libtoolize",
        "rootfs/usr/bin/libtool",
        "rootfs/usr/bin/pgawk-3.1.5",
        "rootfs/usr/bin/yacc",
        "rootfs/usr/bin/flex",
        "rootfs/usr/bin/igawk",
        "rootfs/usr/share/aclocal",
        "rootfs/usr/share/bison",
        "rootfs/usr/share/libtool",
        "rootfs/usr/share/awk"
        ]
});

push @kategorie, ({
    "tag" => "USER_ROOT",
    "order" => "10",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/root/.bash_profile",
        "rootfs/root/.bash_history",
        "rootfs/root/.bash_logout",
        "rootfs/root/.bashrc"
        ]
});

push @kategorie, ({
    "tag" => "NAND",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/nanddump",
    ]
});

push @kategorie, ({
    "tag" => "SANITIZE",
    "order" => "9",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/lib/libstdc.*so.*",   ### lib stdc++
        "rootfs/lib/libasan.so*",
        "rootfs/lib/libubsan.so*",
        ]
});

push @kategorie, ({
    "tag" => "DSL",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/bin/bspatch",
        "rootfs/usr/lib/libbz2.*so.*"
    ]
});

push @kategorie, ({
    "tag" => "LTE",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/bin/bspatch",
        "rootfs/usr/lib/libbz2.*so.*"
    ]
});

# Bash für atomp7. Auf anderen Architekturen ohne Wirkung, da nur für atom7 das
# Übersetzen der Bash eingeschaltet ist.
push @kategorie, ({
    "tag" => "DOCSIS_MODEM",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/lib/libncurses.*so.*",
        "rootfs/bin/bash",
        "rootfs/usr/lib/libreadline.*so.*",
        "rootfs/usr/lib/libhistory.*so.*"
    ]
});

# atomp7 intel wlan (JZ-41629)
push @kategorie, ({
    "tag" => "ATOM_P7_NEUES_WLAN",
    "order" => "3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr",
        "rootfs/usr/lib/crda",
        "rootfs/usr/lib/crda/regulatory.bin",
        "rootfs/usr/lib/libnl*",
        "rootfs/usr/lib/libreg.so",
        "rootfs/usr/lib/libgcrypt.so*",
        "rootfs/usr/lib/libgpg-error.so*",
        "rootfs/usr/sbin/iw",
        "rootfs/usr/sbin/crda"
    ]
});

push @kategorie, ({
    "tag" => "BASIS",
    "order" => "3",
    "arch" => [ "armp7" ],
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/blkid",
        "rootfs/usr/lib/libuuid.so",
        "rootfs/usr/lib/libblkid.so",
        "rootfs/sbin"
    ]
});

my %dirs;
my %files;

##########################################################################################
#
##########################################################################################
sub dbg {
    if ($debug == 1) {
        print STDERR $_[0];
    }
}

##########################################################################################
#
##########################################################################################
sub inarray {
    my ($arrayref, $value) = @_;

    my @array = @{ $arrayref };
    foreach my $val (@array) {
        if ($val eq $value) { return 1; }
    }
    return 0;
}

##########################################################################################
#
##########################################################################################
sub add_file_dir {
    my ( $tag, $file, $arch ) = @_;
    dbg "add_file_dir $tag $file " . print_array($arch) . "\n";

    if (not (-d $file)) { return; }
    my $dir = $file;
    my $hash_dir = $dir;
    $hash_dir =~ s/(.*)\/.*/$1/g;

    if (not defined($dirs{$hash_dir})) {
        $dirs{$hash_dir} = [ ];
    };
    foreach my $i (keys %dirs) {
        dbg "## dir =" . $i . "\n";
        foreach my $entry (@{${dirs}{$i}}) {
            dbg "## " . $entry->{dir} . ", " . join(",", @{$entry->{tag}}) . "\n";
            if ( $entry->{dir} eq $dir ) {
                if ((inarray($entry->{tag}, $tag) == 1) && (compare_arch_refs($entry->{arch}, $arch))) {
                    dbg "## already included\n";
                    return;
                } else {
                    if (is_empty_array($arch) && is_empty_array($entry->{arch})) {
                        dbg "add tag $tag to $dir\n";
                        push $entry->{tag}, ($tag);
                        return;
                    }
                }
            }
        }
    }
    push @{$dirs{$hash_dir}}, ( { "tag" => [$tag], "dir" => $dir, "arch" => $arch } );
    dbg "## add dir=" . $dir . "\n";
}

##########################################################################################
sub is_empty_array {
    my ($arry_ref) = @_;
    if (!defined($arry_ref)) { return 1; }
    if (@{$arry_ref} == 0) { return 1; }
    return 0;
}

##########################################################################################
sub compare_arch_refs {
    my ($a, $b) = @_;
    if (defined($a) && defined($b) && ($a == $b)) { return 1; }
    if (!defined($a) && defined($b)) { return 0; }
    if (!defined($b) && defined($a)) { return 0; }
    if (!defined($a) && !defined($b)) { return 1; }
    return 0;
}

##########################################################################################
sub print_array {
    if (defined($_[0])) { return join(",",@{$_[0]}); }
    else { return "undef"; }
}

##########################################################################################
#
##########################################################################################
sub add_file_file {
    my ( $tag, $file, $arch ) = @_;

    dbg "add_file_file $tag $file " . print_array($arch) . "\n";

    if (-d $file) { return; }
    my $hash_file = $file;
    $hash_file =~ s/\//_/g;

    if (not defined($files{$hash_file})) {
        $files{$hash_file} = [ ];
    };
    foreach my $i (keys %files) {
        dbg "## hash=" . $i . "\n";
        foreach my $ii (@{${files}{$i}}) {
            if (( $ii->{file} eq $file) and ( $ii->{tag} eq $tag ) and compare_arch_refs($ii->{arch},$arch)) {
                dbg "## already included\n";
                return;
            }
        }
    }
    push @{$files{$hash_file}}, ( { "tag" => $tag, "file" => $file, "arch" => $arch } );
    dbg "## file=" . $file . "\n";
}


##########################################################################################
#
##########################################################################################
sub find_kategorie {
    my ( $file ) = @_;

    dbg "########## FILE " . $file . "\n";
    foreach my $i (sort { $a->{order} <=> $b->{order} } @kategorie) {
        dbg "Check Kategorie: " . $i->{tag} . "\n";
        foreach my $inc_dir (@{$i->{include_dirs}}) {
            dbg "Check Include: " . $inc_dir . ": ";
            my $match = "\$file =~ \"" . $inc_dir . "\"";
            if ( eval $match ) {
                dbg "Ja\n";
                my $include = "ja";
                if($#{$i->{exclude_dirs}} >= 0) {
                    foreach my $excl_dir (@{$i->{exclude_dirs}}) {
                        dbg "\tCheck Exclude: " . $excl_dir . ": ";
                        $match = "\$file =~ \"" . $excl_dir . "\"";
                        if ( eval $match ) {
                            dbg "Ja\n";
                            $include = "nein";
                            last;
                        } else {
                            dbg "Nein\n";
                        }
                    }
                }
                if($include eq "ja") {
                    dbg "->include\n";
                    add_file_dir($i->{tag}, $file, $i->{arch});
                    add_file_file($i->{tag}, $file, $i->{arch});
		            last;
                } else {
                    dbg "excluded.\n";
                }
            } else {
                dbg "Nein\n";
            }
        }
    }
    if($file =~ /rootfs/) {
        dbg "[find_kategorie]: " . $file . " konnte keine Kategorie zugeordnet werden.\n";
    }
}

##########################################################################################
#
##########################################################################################
sub read_files {
    my ( $kernel_dir ) = @_;
    my $handle;
    my $count = 0;

   dbg "## open \$handle, \"find \"" . $kernel_dir . "\" |\"\n";
    open $handle, "find " . $kernel_dir . 
        " |" or die "FEHLER: Konnte keine Kernel Treiber finden";

    while(<$handle>) {
        $count++;
        if($count > 25) {
            $count = 0;
            print STDERR ".";
        }
        $_ =~ s/\n//g;
        push @input_files, ( $_ );
    }
    print STDERR "\n";
    close $handle;
}

##########################################################################################
#
##########################################################################################
sub process_files {
    my $count = 0;
    for my $file ( @input_files ) {
        $count++;
        if($count > 25) {
            $count = 0;
            print STDERR "+";
        }
        find_kategorie ($file)
    }
}

##########################################################################################
#
##########################################################################################
sub map_source {
    my ( $type, $source ) = @_;

#    dbg "# map_source: input '" . $source . "'\n";
    $source =~ s/(.*)\/(rootfs.*)/$1\/.\/$2/g;
#    dbg "# map_source: output '" . $source . "'\n";
    return $source;
}

##########################################################################################
#
##########################################################################################
sub map_dest {
    my ( $type, $dest ) = @_;

#    dbg "# map_dest: input '" . $dest . "'\n";
    $dest =~ s/(.*?rootfs)\/(.*)/.\/$2/g;
#    dbg "# map_dest: output '" . $dest . "'\n";
    return $dest;
}


##########################################################################################
#
##########################################################################################
sub write_add_file_dirs {
    my ( $out ) = @_;
    foreach my $i (sort keys %dirs) {
        dbg "## hash=" . $i . "\n";
        my @sorteddirs = sort { $a->{dir} cmp $b->{dir} } @{${dirs}{$i}};
        foreach my $dir (@sorteddirs) {
            my $source = map_source("dir", $dir->{dir});
            my $dest   = map_dest("dir", $dir->{dir});
            if (is_empty_array($dir->{arch})) {
                print $out "[" . join(",", @{$dir->{tag}});
                print $out "] D 777 " . $source . " " . $dest . "\n";
            } else {
                # Das tag Array darf nur einen Eintrag haben
                print $out @{$dir->{tag}}[0];
                print $out "[" . join(",", map { "_" . $_ } @{$dir->{arch}}) . "] D 777 " . $source . " " . $dest . "\n";
            }
        }
    }
}

##########################################################################################
sub build_add_file_line_file {
    my ($file, $out) = @_;
    my $line = "";

    dbg "build_add_file_line_file $file\n";

    if(-l $file) {
        my $source = map_dest("link", $file);
        my $dest   = map_dest("link", readlink $file);
        $line = $line . " L 777 " . $source . " " . $dest . "\n";
    } else {
        my $s_handle;
        my $strip_info = "";
        my $strip_option = "--strip-debug --strip-unneeded";
        if ( open $s_handle, "file \"" . $file . "\" |" ) {
            while(<$s_handle>) {
                if($_ =~ /$FILE_ARCH/) {
                    if($_ =~ /not stripped/) {
                        $strip_info = $file;
                    }
                    if($_ =~ /shared object/) {
                        my $strip_option = $strip_option . " --";
                    }
                    if($_ =~ /executable/) {
                        my $strip_option = $strip_option . " --discard-all --";
                    }
                    
                }
            }
            close $s_handle;
        }
        my $attr = "000";
        if ( -x $file ) {
            if ( -w $file ) {
                $attr = "777";
            } else {
                $attr = "555";
            }
        } else {
            if ( -w $file ) {
                $attr = "666";
            } else {
                $attr = "444";
            }
        }
        my $source = map_source("file", $file);
        my $dest   = map_dest("file", $file);
        $line = $line . " F " . $attr . " " . $source . " " . $dest . "\n";
        if(length($strip_info) > 1) {
            my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size, $atime,$mtime,$ctime,$blksize,$blocks) = stat($strip_info);
            my $vor = $size;
            print $out "##### File before strip: '" . $strip_info . "' " . $size . " Bytes\n";
            if( open $s_handle, ${filesystem} . "/usr/bin/" . lc($TOOL_ARCH) . "-linux-strip --verbose " . $strip_info . " " . $strip_option . " |" ) {
                while(<$s_handle>) {
                    print $out "###" . $_;
                }
                close $s_handle;
            }
            ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size, $atime,$mtime,$ctime,$blksize,$blocks) = stat($strip_info);
            my $nach = $size;
            print $out "##### File after strip: '" . $strip_info . "' " . $size . " Bytes\n";
            print STDERR "[STRIP] " . $dest . " vor " . $vor . " nach " . $nach . " -" . ( 100 - ($nach * 100 / $vor)) . "%\n";
        }
    }
    return $line;
}

##########################################################################################
#
##########################################################################################
sub write_add_file_files {
    my ( $out ) = @_;
    foreach my $i (keys %files) {
        my $trenner = "";
        my $nonarch = 0;

        my $line = build_add_file_line_file(@{${files}{$i}}[0]->{file}, $out);

        # Tag
        foreach my $file (@{${files}{$i}}) {
            # Wenn arch != undef, dann überspringen
            if (defined($file->{arch}) && (@{$file->{arch}} > 0)) { next; }
            if ($nonarch == 0) { print $out "["; $nonarch = 1; }
            print $out $trenner . $file->{tag};
            $trenner = ",";
            dbg "\tfile " . $file->{file} . " TAG " . $file->{tag} . "\n";
        }
        if ($nonarch == 1) { print $out "]"; }
        # Tag Ende

        # Restzeile schreiben
        print $out $line;

        # alle Einträge mit arch != undef durchiterieren
        foreach my $file (@{${files}{$i}}) {
            # Wenn arch == undef, dann überspringen
            if (!defined($file->{arch}) || (@{$file->{arch}} == 0)) { next; }
            print $out $file->{tag} . "[" . join(",", map { "_" . $_ } @{$file->{arch}}) . "]";
            dbg "\tfile(2) " . $file->{file} . " TAG " . $file->{tag} . " arch=" . join(" ", @{$file->{arch}}) . "\n";

            # Restzeile schreiben
            print $out $line;
        }

    }
}

my $out;

if ( defined( $ARGV[0] )) {
    print "Schreibe Datei " . $ARGV[0] . " ...\n";
    open $out, ">" . $ARGV[0] or die "FEHLER: Konnte " . $ARGV[0] . " nicht öffnen\n";
} else {
    $out = <STDOUT>;
}


print "# Read Filesystem\n";
read_files (${filesystem});
process_files ();
print "# add file dirs \n";
write_add_file_dirs($out) ;
print "# add files \n";
write_add_file_files($out) ;
print "# done\n";


#ifndef AVM_SBL_H
#define AVM_SBL_H


#define SBL_MAGIC_STRING               "(c) AVM 2017, SBL Boot Info Magic" \
                                             "(c) AVM 2017, SBL Boot Info Magic" \
                                             "(c) AVM 2017, SBL Boot Info Magic"
struct sbl_boot_status{
    unsigned char magic_str[256]; /* Magic */
    unsigned int tz_boot_index; /* 0 or 1, depending on chosen TZ */
    unsigned int tz_boot_successful; /* kernel writes a 1 to mark successfull boot */
    unsigned int tz_verified[2]; /* TZ passed Verification */
    unsigned int tz_version[2]; /* Version of TZ */
    unsigned int tz_modified[2]; /* Modified Status of TZ */
    unsigned int eva_boot_index; /* 0 or 1, depending on chosen eva */
    unsigned int eva_boot_successful; /* kernel writes a 1 to mark successfull boot */
    unsigned int eva_verified[2]; /* eva passed Verification */
    unsigned int eva_version[2]; /* Version of eva */
    unsigned int eva_modified[2]; /* Modified Status of eva*/
    unsigned int sbl_version;
    unsigned int sbl_modified;
};
struct sbl_boot_status * sbl_reboot_info;

#endif

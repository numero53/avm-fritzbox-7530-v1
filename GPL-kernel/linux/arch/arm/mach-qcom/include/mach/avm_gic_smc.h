#ifndef _AVM_GIC_SMC_H_
#define _AVM_GIC_SMC_H_

/* AVM Trustzone Commands */
#define SCM_SVC_AVM         0xA0


#define AVM_SMC_GET_VERSION     0x28000000
#define AVM_SMC_FIQ_MODE        0x28001000
#define AVM_SMC_GIC_ACK         0x28002000
#define AVM_SMC_GIC_EOI         0x28003000
#define AVM_SMC_FIQ_FOR_TZ      0x28004000
#define AVM_SMC_GICC_READ       0x28005000
#define AVM_SMC_GICC_WRITE      0x28006000
#define AVM_SMC_GICD_READ       0x28007000
#define AVM_SMC_GICD_WRITE      0x28008000


#define SCM_INTERRUPTED		 1

#endif

/*------------------------------------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------------------------------------*/

#ifndef _AVM_GPIO_H_
#define _AVM_GPIO_H_

#define GPIO_OK                 (0)
#define GPIO_FAIL               (-1)

#include <linux/avm_hw_config_def.h>
#include <linux/io.h>


enum _hw_gpio_function
{
	GPIO_PIN = AVM_DEF_HW_FUNCTION_GPIO_PIN,
	FUNCTION_PINMUX1 = AVM_DEF_HW_FUNCTION_PINMUX1,
	FUNCTION_PINMUX2 = AVM_DEF_HW_FUNCTION_PINMUX2,
	FUNCTION_PINMUX3 = AVM_DEF_HW_FUNCTION_PINMUX3,
	FUNCTION_PINMUX4 = AVM_DEF_HW_FUNCTION_PINMUX4,
	FUNCTION_PINMUX5 = AVM_DEF_HW_FUNCTION_PINMUX5,
	FUNCTION_PINMUX6 = AVM_DEF_HW_FUNCTION_PINMUX6,
	FUNCTION_PINMUX7 = AVM_DEF_HW_FUNCTION_PINMUX7,
	FUNCTION_PINMUX8 = AVM_DEF_HW_FUNCTION_PINMUX8,
	FUNCTION_PINMUX9 = AVM_DEF_HW_FUNCTION_PINMUX9,
	FUNCTION_PINMUX10 = AVM_DEF_HW_FUNCTION_PINMUX10,
	FUNCTION_PINMUX11 = AVM_DEF_HW_FUNCTION_PINMUX11,
	FUNCTION_PINMUX12 = AVM_DEF_HW_FUNCTION_PINMUX12,
	FUNCTION_PINMUX13 = AVM_DEF_HW_FUNCTION_PINMUX13,
	FUNCTION_PINMUX14 = AVM_DEF_HW_FUNCTION_PINMUX14,
	FUNCTION_PINMUX15 = AVM_DEF_HW_FUNCTION_PINMUX15,
	FUNCTION_PIN_NOCHANGE = AVM_DEF_HW_FUNCTION_PIN_NOCHANGE,
};

enum _hw_gpio_direction
{
    GPIO_INPUT_PIN = 0,
    GPIO_OUTPUT_PIN = 1
};

enum _hw_gpio_polarity {
    AVM_GPIO_ACTIVE_HIGH = 0,
    AVM_GPIO_ACTIVE_LOW  = 1
};

enum _hw_gpio_config {
	PINCONF_PARAM_PULLDOWN       = 0,
	PINCONF_PARAM_PULLUP         = 1,
	PINCONF_PARAM_OPEN_DRAIN     = 2,
    PINCONF_PARAM_HIGHPOWER      = 3,
	PINCONF_PARAM_DRIVE_STRENGTH = 4,
    PINCONF_PARAM_GPIO_PU_RES    = 5,
	PINCONF_PARAM_NOCHANGE       = 0xFF
};

typedef enum _hw_gpio_function  GPIO_MODE;
typedef enum _hw_gpio_direction GPIO_DIR;
typedef enum _hw_gpio_polarity  GPIO_POLAR;

int dakota_gpio_ctrl(unsigned int gpio_pin, enum _hw_gpio_function pin_mode, enum _hw_gpio_direction pin_dir);
int dakota_gpio_out_bit(unsigned int gpio_pin, int value);
int dakota_gpio_out_bit_no_sched(unsigned int gpio_pin, int value);
int dakota_gpio_in_bit(unsigned int gpio_pin);
int dakota_gpio_pinconfig(unsigned int gpio_pin, enum _hw_gpio_config param, unsigned int set);
void __iomem *dakota_gpio_get_membase(unsigned int gpio_pin);

static inline int dakota_gpio_in_bit_fast(void __iomem *reg){
    return ((readl_relaxed(reg + 4)) & 1);
}
#endif/*--- #ifndef _AVM_GPIO_H_ ---*/


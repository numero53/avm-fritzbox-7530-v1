#include <linux/module.h>
#include <linux/cpufreq.h>
#include <clocksource/arm_arch_timer.h>
/*--- #include <asm/performance.h> ---*/
/*--- #include <linux/kernel.h> ---*/


unsigned int ipq40xx_get_cyclefreq(void) {
    return (arch_timer_get_rate());
}
EXPORT_SYMBOL(ipq40xx_get_cyclefreq);

/**
 * Liest den generic timer (CNTPCT) / arch timer.
 * Wird sehr früh initialisiert und läuft stetig mit 48MHz
 */
unsigned int ipq40xx_get_cycles(void) {
    u64 timeval;
    asm volatile("MRRC p15, 0, %Q0, %R0, c14 ": "=r"(timeval));
    return (unsigned int)timeval;
}
EXPORT_SYMBOL(ipq40xx_get_cycles);


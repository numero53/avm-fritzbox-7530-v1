/*
 *  Atheros AR724X PCI host controller driver
 *
 *  Copyright (C) 2011 René Bolldorf <xsecute@googlemail.com>
 *  Copyright (C) 2009-2011 Gabor Juhos <juhosg@openwrt.org>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/irq.h>
#include <linux/pci.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/avm_hw_config.h>
#include <linux/avm_kernel_config.h>
#include <linux/kdebug.h>
#include <linux/simple_proc.h>
#include <linux/proc_fs.h>
#include <asm/mach-ath79/ath79.h>
#include <asm/mach-ath79/ar71xx_regs.h>
#include <asm/mach-ath79/ar71xx_pci_regs.h>
#include <asm/mach-ath79/avm_atheros.h>

#define AR724X_PCI_REG_RESET		0x18
#define AR724X_PCI_REG_INT_STATUS	0x4c
#define AR724X_PCI_REG_INT_MASK		0x50

#define AR724X_PCI_RESET_LINK_UP	BIT(0)

#define AR724X_PCI_INT_DEV0		BIT(14)

#define AR724X_PCI_IRQ_COUNT		1

#define AR7240_BAR0_WAR_VALUE	0xffff

#define AR724X_PCI_CMD_INIT	(PCI_COMMAND_MEMORY |		\
				 PCI_COMMAND_MASTER |		\
				 PCI_COMMAND_INVALIDATE |	\
				 PCI_COMMAND_PARITY |		\
				 PCI_COMMAND_SERR |		\
				 PCI_COMMAND_FAST_BACK)

#define PCIE_INT_STATUS_LINK_DOWN_MASK                               0x08000000
#define PCIE_INT_STATUS_LINK_REQ_RST_MASK                            0x04000000
#define PCIE_INT_STATUS_SYS_ERR_MASK                                 0x00002000
#define PCIE_INT_STATUS_AER_INT_MASK                                 0x00000800
#define PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_MASK                   0x00000010
#define PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_MASK                      0x00000008
#define PCIE_INT_STATUS_FATAL_ERR_MASK                               0x00000004

// Following are FATAL error indications per Register Specification
#define AR724X_PCI_ERROR_MASK   (PCIE_INT_STATUS_LINK_DOWN_MASK		| \
				PCIE_INT_STATUS_LINK_REQ_RST_MASK	| \
				PCIE_INT_STATUS_SYS_ERR_MASK		| \
				PCIE_INT_STATUS_AER_INT_MASK		| \
				PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_MASK	| \
				PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_MASK	| \
				PCIE_INT_STATUS_FATAL_ERR_MASK)

struct ar724x_pci_controller {
	void __iomem *devcfg_base;
	void __iomem *ctrl_base;
	void __iomem *crp_base;

	int irq;
	int irq_base;

	bool link_up;
	bool bar0_is_cached;
	u32  bar0_value;

	struct pci_controller pci_controller;
	struct resource io_res;
	struct resource mem_res;

	struct notifier_block die_info_nb;
};

static inline bool ar724x_pci_check_link(struct ar724x_pci_controller *apc)
{
	u32 reset;

	reset = __raw_readl(apc->ctrl_base + AR724X_PCI_REG_RESET);
	return reset & AR724X_PCI_RESET_LINK_UP;
}

static inline struct ar724x_pci_controller *
pci_bus_to_ar724x_controller(struct pci_bus *bus)
{
	struct pci_controller *hose;

	hose = (struct pci_controller *) bus->sysdata;
	return container_of(hose, struct ar724x_pci_controller, pci_controller);
}

static int ar724x_pci_local_write(struct ar724x_pci_controller *apc,
				  int where, int size, u32 value)
{
	void __iomem *base;
	u32 data;
	int s;

	WARN_ON(where & (size - 1));

	if (!apc->link_up)
		return PCIBIOS_DEVICE_NOT_FOUND;

	base = apc->crp_base;
	data = __raw_readl(base + (where & ~3));

	switch (size) {
	case 1:
		s = ((where & 3) * 8);
		data &= ~(0xff << s);
		data |= ((value & 0xff) << s);
		break;
	case 2:
		s = ((where & 2) * 8);
		data &= ~(0xffff << s);
		data |= ((value & 0xffff) << s);
		break;
	case 4:
		data = value;
		break;
	default:
		return PCIBIOS_BAD_REGISTER_NUMBER;
	}

	__raw_writel(data, base + (where & ~3));
	/* flush write */
	__raw_readl(base + (where & ~3));

	return PCIBIOS_SUCCESSFUL;
}

static int ar724x_pci_read(struct pci_bus *bus, unsigned int devfn, int where,
			    int size, uint32_t *value)
{
	struct ar724x_pci_controller *apc;
	void __iomem *base;
	u32 data;

	apc = pci_bus_to_ar724x_controller(bus);
	if (!apc->link_up)
		return PCIBIOS_DEVICE_NOT_FOUND;

	if (devfn)
		return PCIBIOS_DEVICE_NOT_FOUND;

	base = apc->devcfg_base;
	data = __raw_readl(base + (where & ~3));

	switch (size) {
	case 1:
		if (where & 1)
			data >>= 8;
		if (where & 2)
			data >>= 16;
		data &= 0xff;
		break;
	case 2:
		if (where & 2)
			data >>= 16;
		data &= 0xffff;
		break;
	case 4:
		break;
	default:
		return PCIBIOS_BAD_REGISTER_NUMBER;
	}

	if (where == PCI_BASE_ADDRESS_0 && size == 4 &&
	    apc->bar0_is_cached) {
		/* use the cached value */
		*value = apc->bar0_value;
	} else {
		*value = data;
	}

	return PCIBIOS_SUCCESSFUL;
}

static int ar724x_pci_write(struct pci_bus *bus, unsigned int devfn, int where,
			     int size, uint32_t value)
{
	struct ar724x_pci_controller *apc;
	void __iomem *base;
	u32 data;
	int s;

	apc = pci_bus_to_ar724x_controller(bus);
	if (!apc->link_up)
		return PCIBIOS_DEVICE_NOT_FOUND;

	if (devfn)
		return PCIBIOS_DEVICE_NOT_FOUND;

	if (soc_is_ar7240() && where == PCI_BASE_ADDRESS_0 && size == 4) {
		if (value != 0xffffffff) {
			/*
			 * WAR for a hw issue. If the BAR0 register of the
			 * device is set to the proper base address, the
			 * memory space of the device is not accessible.
			 *
			 * Cache the intended value so it can be read back,
			 * and write a SoC specific constant value to the
			 * BAR0 register in order to make the device memory
			 * accessible.
			 */
			apc->bar0_is_cached = true;
			apc->bar0_value = value;

			value = AR7240_BAR0_WAR_VALUE;
		} else {
			apc->bar0_is_cached = false;
		}
	}

	base = apc->devcfg_base;
	data = __raw_readl(base + (where & ~3));

	switch (size) {
	case 1:
		s = ((where & 3) * 8);
		data &= ~(0xff << s);
		data |= ((value & 0xff) << s);
		break;
	case 2:
		s = ((where & 2) * 8);
		data &= ~(0xffff << s);
		data |= ((value & 0xffff) << s);
		break;
	case 4:
		data = value;
		break;
	default:
		return PCIBIOS_BAD_REGISTER_NUMBER;
	}

	__raw_writel(data, base + (where & ~3));
	/* flush write */
	__raw_readl(base + (where & ~3));

	return PCIBIOS_SUCCESSFUL;
}

static struct pci_ops ar724x_pci_ops = {
	.read	= ar724x_pci_read,
	.write	= ar724x_pci_write,
};

#ifdef CONFIG_AVM_PCI_RECOVERY
static void ath_pci_reset_work_func(struct work_struct *work)
{
	ath_pci_reset();
}
DECLARE_WORK(ath_pci_reset_work, ath_pci_reset_work_func);
#endif

static void ar724x_pci_error_handler(struct irq_desc *desc, u32 error)
{
	struct ar724x_pci_controller *apc;
#ifdef CONFIG_AVM_PCI_RECOVERY
	struct pci_dev *d, *tmp;
	int should_recover = 1;
#endif

	apc = irq_desc_get_handler_data(desc);

	pr_err("PCI Error occurred: 0x%08x\n", error);

	/*
	 * Odly the irq_data associated with desc is describes
	 * the parent irq_handler and not the apc irq handler.
	 *
	 * Therefore we cant simply reuse this here.
	 */

	/*
	 * Mask these off, else the interrupt does not stop
	 * the handler is invoked repeatedly and is not able
	 * to panic.
	 *
	 * Because we don't have the correct irq_data, do this
	 * manually.
	 */
	__raw_writel(0, apc->ctrl_base + AR724X_PCI_REG_INT_MASK);

	apc->link_up = 0;

#ifdef CONFIG_AVM_PCI_RECOVERY
	/*
	 * Notify all devices on this bus, that an error occurred. Use a permanent
	 * failure at the moment, as recovery means a complete reset of the RC.
	 * This also means that all devices are gone, which indicates the permanence
	 */
	list_for_each_entry_safe(d, tmp, &apc->pci_controller.bus->devices, bus_list) {
		enum pci_ers_result res;

		d->error_state = pci_channel_io_perm_failure;

		if (!d->driver)
			continue;

		if (d->driver->err_handler && d->driver->err_handler->error_detected)
			res = d->driver->err_handler->error_detected(d, pci_channel_io_perm_failure);
		else
			res = PCI_ERS_RESULT_NONE;

		/*
		 * TODO: currently everything will result in a complet RC reset, we should
		 * allow the driver more control over what to do
		 */
		if (res == PCI_ERS_RESULT_NONE) {
			pr_err("PCI driver %s does not support recovery\n", d->driver->name);
			should_recover = 0;
		}
	}

	if (should_recover)
		schedule_work(&ath_pci_reset_work);
	else
#endif
		die("PCI Error", get_irq_regs());
}

static void ar724x_pci_irq_handler(struct irq_desc *desc)
{
	struct ar724x_pci_controller *apc;
	void __iomem *base;
	u32 pending;

	apc = irq_desc_get_handler_data(desc);
	base = apc->ctrl_base;

	pending = __raw_readl(base + AR724X_PCI_REG_INT_STATUS) &
		  __raw_readl(base + AR724X_PCI_REG_INT_MASK);

	if (pending & AR724X_PCI_ERROR_MASK)
		ar724x_pci_error_handler(desc, pending & AR724X_PCI_ERROR_MASK);

	else if (pending & AR724X_PCI_INT_DEV0)
		generic_handle_irq(apc->irq_base + 0);

	else
		spurious_interrupt();
}

static void ar724x_pci_irq_unmask(struct irq_data *d)
{
	struct ar724x_pci_controller *apc;
	void __iomem *base;
	int offset;
	u32 t;

	apc = irq_data_get_irq_chip_data(d);
	base = apc->ctrl_base;
	offset = apc->irq_base - d->irq;

	switch (offset) {
	case 0:
		t = __raw_readl(base + AR724X_PCI_REG_INT_MASK);
		__raw_writel(t | (AR724X_PCI_INT_DEV0 | AR724X_PCI_ERROR_MASK),
			     base + AR724X_PCI_REG_INT_MASK);
		/* flush write */
		__raw_readl(base + AR724X_PCI_REG_INT_MASK);
	}
}

static void ar724x_pci_irq_mask(struct irq_data *d)
{
	struct ar724x_pci_controller *apc;
	void __iomem *base;
	int offset;
	u32 t;

	apc = irq_data_get_irq_chip_data(d);
	base = apc->ctrl_base;
	offset = apc->irq_base - d->irq;

	switch (offset) {
	case 0:
		t = __raw_readl(base + AR724X_PCI_REG_INT_MASK);
		t &= ~(AR724X_PCI_INT_DEV0 | AR724X_PCI_ERROR_MASK);
		__raw_writel(t, base + AR724X_PCI_REG_INT_MASK);

		/* flush write */
		__raw_readl(base + AR724X_PCI_REG_INT_MASK);

		t = __raw_readl(base + AR724X_PCI_REG_INT_STATUS);
		t |= AR724X_PCI_INT_DEV0;
		t &= ~AR724X_PCI_ERROR_MASK;
		__raw_writel(t, base + AR724X_PCI_REG_INT_STATUS);

		/* flush write */
		__raw_readl(base + AR724X_PCI_REG_INT_STATUS);
	}
}

static struct irq_chip ar724x_pci_irq_chip = {
	.name		= "AR724X PCI ",
	.irq_mask	= ar724x_pci_irq_mask,
	.irq_unmask	= ar724x_pci_irq_unmask,
	.irq_mask_ack	= ar724x_pci_irq_mask,
};

static void ar724x_pci_irq_init(struct ar724x_pci_controller *apc,
				int id)
{
	void __iomem *base;
	int i;

	base = apc->ctrl_base;

	__raw_writel(0, base + AR724X_PCI_REG_INT_MASK);
	__raw_writel(0, base + AR724X_PCI_REG_INT_STATUS);

	apc->irq_base = ATH79_PCI_IRQ_BASE + (id * AR724X_PCI_IRQ_COUNT);

	for (i = apc->irq_base;
	     i < apc->irq_base + AR724X_PCI_IRQ_COUNT; i++) {
		irq_set_chip_and_handler(i, &ar724x_pci_irq_chip,
					 handle_level_irq);
		irq_set_chip_data(i, apc);
	}

	irq_set_chained_handler_and_data(apc->irq, ar724x_pci_irq_handler,
					 apc);
}

static int die_info_notify(struct notifier_block *self, unsigned long dummy, void *param) {
	struct ar724x_pci_controller *apc = container_of(self, struct ar724x_pci_controller, die_info_nb);

	unsigned long int_status = ath_reg_rd(apc->ctrl_base + 0x4c);
	unsigned long aer = ath_reg_rd(apc->ctrl_base + 0x04);
	unsigned long uer = ath_reg_rd(apc->crp_base + 0x104);
	unsigned long cer = ath_reg_rd(apc->crp_base + 0x110);
	unsigned long debug = ath_reg_rd(apc->ctrl_base + 0x1c);
	unsigned long mac_phy = ath_reg_rd(apc->ctrl_base + 0x30);
	unsigned long phy_mac = ath_reg_rd(apc->ctrl_base + 0x34);
	unsigned long err_cnt = ath_reg_rd(apc->ctrl_base + 0x54);

	pr_emerg("PCI: int=%08lx aer=%08lx uer=%08lx cer=%08lx debug=%08lx "
		"mac_phy=%08lx phy_mac=%08lx err_cnt=%08lx\n",
		int_status, aer, uer, cer, debug, mac_phy, phy_mac, err_cnt);

	return NOTIFY_OK;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void ar724x_pci_board_init(struct ar724x_pci_controller *apc)
{
	uint32_t cmd;
	int gpio_pcie_reset = 0;
	int gpio_device_reset = 0;

	avm_get_hw_config(AVM_HW_CONFIG_VERSION, "gpio_avm_pcie_reset", &gpio_pcie_reset, NULL);
	avm_get_hw_config(AVM_HW_CONFIG_VERSION, "gpio_avm_peregrine_reset", &gpio_device_reset, NULL);

	/*----------------------------------------------------------------------*\
	 * PCIE aus dem Reset holen
	\*----------------------------------------------------------------------*/
#if defined(CONFIG_SOC_AR724X)
	ath_reg_rmw_set(ATH_RESET,RST_RESET_PCIE_PHY_SERIAL_SET(1));
	mdelay(100);
#endif

	ath_reg_rmw_set(ATH_RESET, RST_RESET_PCIE_PHY_RESET_SET(1));
	mdelay(10);

	ath_reg_rmw_set(ATH_RESET, RST_RESET_PCIE_RESET_SET(1));
	mdelay(10);
#if defined(CONFIG_SOC_QCN550X)
	ath_reg_rmw_set(ATH_RESET_2, RST_RESET_PCIE_PHY_RESET_SET(1));
	mdelay(10);
	ath_reg_rmw_set(ATH_RESET_2, RST_RESET_PCIE_RESET_SET(1));
	mdelay(10);
#endif

#if ! defined(CONFIG_SOC_AR724X)
	ath_reg_rmw_clear(RST_MISC2_ADDRESS, RST_MISC2_PERSTN_RCPHY_SET(1));
	mdelay(10);

	ath_reg_wr_nf(apc->ctrl_base + AR724X_PCI_REG_RESET, 0);	// Put endpoint in reset
	if (gpio_pcie_reset)
		ath_avm_gpio_out_bit(gpio_pcie_reset, 0);
	if (gpio_device_reset)
		ath_avm_gpio_out_bit(gpio_device_reset, 0);
	mdelay(100);

	ath_reg_rmw_set(RST_MISC2_ADDRESS, RST_MISC2_PERSTN_RCPHY_SET(1));
	mdelay(10);
#endif

#if defined(CONFIG_SOC_AR724X)
	ath_reg_rmw_clear(ATH_RESET,RST_RESET_PCIE_PHY_SERIAL_SET(1)); 
	mdelay(10);
#endif

	ath_reg_rmw_clear(ATH_RESET, RST_RESET_PCIE_PHY_RESET_SET(1));
	mdelay(10);

	ath_reg_rmw_clear(ATH_RESET, RST_RESET_PCIE_RESET_SET(1));
	mdelay(10);
#if defined(CONFIG_SOC_QCN550X)
	ath_reg_rmw_clear(ATH_RESET_2, RST_RESET_PCIE_PHY_RESET_SET(1));
	mdelay(10);
	ath_reg_rmw_clear(ATH_RESET_2, RST_RESET_PCIE_RESET_SET(1));
	mdelay(10);
#endif

	ath_reg_wr_nf(apc->ctrl_base, PCIE_APP_PCIE_BAR_MSN_SET(1) |
					PCIE_APP_CFG_BE_SET(0xf) |
					PCIE_APP_SLV_RESP_ERR_MAP_SET(0x3f) |
					PCIE_APP_LTSSM_ENABLE_SET(1));

	/* TODO: Commenting this out doesn't seem to change anything.
	 *       Can we get rid of this local_write stuff?
	 */
	cmd = PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER | PCI_COMMAND_INVALIDATE |
	      PCI_COMMAND_PARITY|PCI_COMMAND_SERR|PCI_COMMAND_FAST_BACK;

	/* === Emulated Link is up for this commands === */
	apc->link_up = 1;
	/* rc1 */
	ar724x_pci_local_write(apc, PCI_COMMAND, 4, cmd);
	ar724x_pci_local_write(apc, 0x20, 4, 0x1ff01000);
	ar724x_pci_local_write(apc, 0x24, 4, 0x1ff01000);
#if defined(CONFIG_SOC_QCN550X)
	/* rc2 */
	ar724x_pci_local_write(apc, 0x70c, 4, 0x1b403200);
#endif
	apc->link_up = 0;

	ath_reg_wr_nf(apc->ctrl_base + AR724X_PCI_REG_RESET, 4);
	if (gpio_pcie_reset)
		ath_avm_gpio_out_bit(gpio_pcie_reset, 1);
	if (gpio_device_reset)
		ath_avm_gpio_out_bit(gpio_device_reset, 1);
	mdelay(100);

	/*
	 * Check if the WLAN PCI-E H/W is present, If the
	 * WLAN H/W is not present, skip the PCI platform
	 * initialization code and return
	 */
	if (((ath_reg_rd(apc->ctrl_base + AR724X_PCI_REG_RESET)) & 0x1) == 0x0)
		pr_err("<Warning : PCIe WLAN Module not found !!!>\n");

	apc->die_info_nb.notifier_call = die_info_notify;
	apc->die_info_nb.priority = INT_MAX;
	register_die_notifier(&apc->die_info_nb);
}


static int ar724x_pci_probe(struct platform_device *pdev)
{
	struct ar724x_pci_controller *apc;
	struct resource *res;
	int id;
	int ret;

	if (pdev->dev.of_node) {
		ret = of_alias_get_id(pdev->dev.of_node, "pci");
		if (ret >= 0)
			pdev->id = ret;
	}

	id = pdev->id;
	if (id == -1)
		id = 0;

	apc = devm_kzalloc(&pdev->dev, sizeof(struct ar724x_pci_controller),
			    GFP_KERNEL);
	if (!apc)
		return -ENOMEM;

	dev_set_drvdata(&pdev->dev, apc);

	res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "ctrl_base");
	apc->ctrl_base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(apc->ctrl_base))
		return PTR_ERR(apc->ctrl_base);

	res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "cfg_base");
	apc->devcfg_base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(apc->devcfg_base))
		return PTR_ERR(apc->devcfg_base);

	res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "crp_base");
	apc->crp_base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(apc->crp_base))
		return PTR_ERR(apc->crp_base);

	apc->irq = platform_get_irq(pdev, 0);
	if (apc->irq < 0)
		return -EINVAL;

	res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "io_base");
	if (!res)
		return -EINVAL;

	apc->io_res.parent = res;
	apc->io_res.name = "PCI IO space";
	apc->io_res.start = res->start;
	apc->io_res.end = res->end;
	apc->io_res.flags = IORESOURCE_IO;

	res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "mem_base");
	if (!res)
		return -EINVAL;

	apc->mem_res.parent = res;
	apc->mem_res.name = "PCI memory space";
	apc->mem_res.start = res->start;
	apc->mem_res.end = res->end;
	apc->mem_res.flags = IORESOURCE_MEM;

	apc->pci_controller.pci_ops = &ar724x_pci_ops;
	apc->pci_controller.io_resource = &apc->io_res;
	apc->pci_controller.mem_resource = &apc->mem_res;

	ar724x_pci_board_init(apc);

	apc->link_up = ar724x_pci_check_link(apc);
	if (!apc->link_up)
		dev_warn(&pdev->dev, "PCIe link is down\n");

	ar724x_pci_irq_init(apc, id);

	ar724x_pci_local_write(apc, PCI_COMMAND, 4, AR724X_PCI_CMD_INIT);

	register_pci_controller(&apc->pci_controller);

	return 0;
}

static int ar724x_pci_remove(struct platform_device *pdev)
{
	struct ar724x_pci_controller *apc;

	apc = dev_get_drvdata(&pdev->dev);

	unregister_pci_controller(&apc->pci_controller);
	devm_kfree(&pdev->dev, apc);

	unregister_die_notifier(&apc->die_info_nb);

	return 0;
}

static const struct of_device_id ath79_pci_of_match[] = {
	{.compatible = "qca,ar724x-pci", },
	{ },
};

static struct platform_driver ar724x_pci_driver = {
	.probe = ar724x_pci_probe,
	.remove = ar724x_pci_remove,
	.driver = {
		.name = "ar724x-pci",
		.of_match_table = ath79_pci_of_match,
	},
};

static int __init ar724x_pci_init(void)
{
	return platform_driver_register(&ar724x_pci_driver);
}

subsys_initcall(ar724x_pci_init);

#ifdef CONFIG_AVM_PCI_RECOVERY

void ath_pci_down(void)
{
	pr_err("Unloading PCI platform driver...\n");

	platform_driver_unregister(&ar724x_pci_driver);
	pr_err("PCI platform driver unloaded\n");
}

void ath_pci_up(void)
{
	pr_err("Reloading PCI platform driver...\n");

	pcibios_reset_busno();
	platform_driver_register(&ar724x_pci_driver);
	pr_err("PCI platform driver reloaded\n");
}

void ath_pci_reset(void)
{
	ath_pci_down();
	ath_pci_up();
}

static int avm_debug_pci_recovery_write(char *string, void *reg)
{
	int gpio_pcie_reset = 0;
	int gpio_device_reset = 0;

	avm_get_hw_config(AVM_HW_CONFIG_VERSION, "gpio_avm_pcie_reset", &gpio_pcie_reset, NULL);
	avm_get_hw_config(AVM_HW_CONFIG_VERSION, "gpio_avm_peregrine_reset", &gpio_device_reset, NULL);

	pr_err("[ath_pci] Kill pci wlan device...\n");

	if (gpio_pcie_reset)
		ath_avm_gpio_out_bit(gpio_pcie_reset, 0);
	if (gpio_device_reset)
		ath_avm_gpio_out_bit(gpio_device_reset, 0);

	return 0;
}

static int __init avm_debug_pci_recovery_init(void)
{
	proc_mkdir("avm/debug", NULL);

	add_simple_proc_file("avm/debug/pci_recovery", avm_debug_pci_recovery_write, NULL, NULL);

	return 0;
}

late_initcall(avm_debug_pci_recovery_init);
#endif

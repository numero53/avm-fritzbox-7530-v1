/*
 *  Atheros AR71XX/AR724X/AR913X specific setup
 *
 *  Copyright (C) 2010-2011 Jaiganesh Narayanan <jnarayanan@atheros.com>
 *  Copyright (C) 2008-2011 Gabor Juhos <juhosg@openwrt.org>
 *  Copyright (C) 2008 Imre Kaloz <kaloz@openwrt.org>
 *
 *  Parts of this file are based on Atheros' 2.6.15/2.6.31 BSP
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/bootmem.h>
#include <linux/err.h>
#include <linux/clk.h>
#include <linux/of_platform.h>
#include <linux/of_fdt.h>
#include <linux/env.h>

#include <asm/bootinfo.h>
#include <asm/idle.h>
#include <asm/time.h>		/* for mips_hpt_frequency */
#include <asm/reboot.h>		/* for _machine_{restart,halt} */
#include <asm/mips_machine.h>
#include <asm/traps.h>
#include <asm/prom.h>
#include <asm/fw/fw.h>

#include <linux/serial.h>
#include <linux/serial_core.h>
#include <linux/serial_reg.h>
#include <linux/serial_8250.h>
#include <linux/console.h>
#include <linux/types.h>
#include <linux/string.h>

#include <linux/avm_kernel_config.h>

#include <asm/mach-ath79/avm_atheros.h>
#include <asm/mach-ath79/ath79.h>
#include <asm/mach-ath79/ar71xx_regs.h>
#include "common.h"
#include "dev-common.h"
#include "machtypes.h"
#include <linux/ath79_wlan.h>

#define ATH79_SYS_TYPE_LEN	64

#define AR71XX_BASE_FREQ	40000000
#define AR724X_BASE_FREQ	5000000
#define AR913X_BASE_FREQ	5000000

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
extern void ath_savelog_to_ram(void);
extern void set_reboot_status_to_NMI(void);
extern void set_reboot_status_to_NMI_WA(void);
extern void set_reboot_status_to_BusError(void);
extern void set_reboot_status_to_SoftReboot(void);

extern void qca955x_setup(void);
extern void ar934x_setup(void);
extern void ar724x_setup(void);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static char ath79_sys_type[ATH79_SYS_TYPE_LEN];
extern char __owrt_dtb[];

void ath79_restart(char *command) {
    unsigned int val = (1<<0);

    if (command) {
        if ( ! strncmp(command, "watchdog", sizeof("watchdog") - 1)) {
            val = (1<<1);
            set_reboot_status_to_NMI();
            ath_savelog_to_ram();
        } else if ( ! strncmp(command, "nmi_workaround", sizeof("nmi_workaround") - 1)) {
            val = (1<<2);
            set_reboot_status_to_NMI_WA();
            ath_savelog_to_ram();
        } else if ( ! strncmp(command, "bus_error", sizeof("bus_error") - 1)) {
            set_reboot_status_to_BusError();
            ath_savelog_to_ram();
        }
    } else {
        set_reboot_status_to_SoftReboot();
        if(oops_in_progress) {
            /*--- wir kommen aus panic() ---*/
            ath_savelog_to_ram();
        }
    }
#if !defined(CONFIG_SOC_AR724X)
    ath_reg_wr(ATH_RESET_BASE + ATH_RESET_REG_STICKY, val);   /*--- save-Rebootstatus ---*/
#endif/*--- #if !defined(CONFIG_SOC_AR724X) ---*/

	ath79_device_reset_set(ATH_RESET_FULL_CHIP);
   	for (;;) {
		if (cpu_wait)
			cpu_wait();
    }
}
EXPORT_SYMBOL(ath79_restart);

static void ath79_halt(void)
{
	while (1)
		cpu_wait();
}

static void __init ath79_detect_sys_type(void)
{
	char *chip = "????";
	u32 id;
	u32 major;
	u32 minor;
	u32 rev = 0;
	u32 ver = 1;

	id = ath79_reset_rr(AR71XX_RESET_REG_REV_ID);
	major = id & REV_ID_MAJOR_MASK;

	switch (major) {
	case REV_ID_MAJOR_AR71XX:
		minor = id & AR71XX_REV_ID_MINOR_MASK;
		rev = id >> AR71XX_REV_ID_REVISION_SHIFT;
		rev &= AR71XX_REV_ID_REVISION_MASK;
		switch (minor) {
		case AR71XX_REV_ID_MINOR_AR7130:
			ath79_soc = ATH79_SOC_AR7130;
			chip = "7130";
			break;

		case AR71XX_REV_ID_MINOR_AR7141:
			ath79_soc = ATH79_SOC_AR7141;
			chip = "7141";
			break;

		case AR71XX_REV_ID_MINOR_AR7161:
			ath79_soc = ATH79_SOC_AR7161;
			chip = "7161";
			break;
		}
		break;

	case REV_ID_MAJOR_AR7240:
		ath79_soc = ATH79_SOC_AR7240;
		chip = "7240";
		rev = id & AR724X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR7241:
		ath79_soc = ATH79_SOC_AR7241;
		chip = "7241";
		rev = id & AR724X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR7242:
		ath79_soc = ATH79_SOC_AR7242;
		chip = "7242";
		rev = id & AR724X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR913X:
		minor = id & AR913X_REV_ID_MINOR_MASK;
		rev = id >> AR913X_REV_ID_REVISION_SHIFT;
		rev &= AR913X_REV_ID_REVISION_MASK;
		switch (minor) {
		case AR913X_REV_ID_MINOR_AR9130:
			ath79_soc = ATH79_SOC_AR9130;
			chip = "9130";
			break;

		case AR913X_REV_ID_MINOR_AR9132:
			ath79_soc = ATH79_SOC_AR9132;
			chip = "9132";
			break;
		}
		break;

	case REV_ID_MAJOR_AR9330:
		ath79_soc = ATH79_SOC_AR9330;
		chip = "9330";
		rev = id & AR933X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR9331:
		ath79_soc = ATH79_SOC_AR9331;
		chip = "9331";
		rev = id & AR933X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR9341:
		ath79_soc = ATH79_SOC_AR9341;
		chip = "9341";
		rev = id & AR934X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR9342:
		ath79_soc = ATH79_SOC_AR9342;
		chip = "9342";
		rev = id & AR934X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR9344:
		ath79_soc = ATH79_SOC_AR9344;
		chip = "9344";
		rev = id & AR934X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_QCA9533_V2:
		ver = 2;
		ath79_soc_rev = 2;
		/* drop through */

	case REV_ID_MAJOR_QCA9533:
		ath79_soc = ATH79_SOC_QCA9533;
		chip = "9533";
		rev = id & QCA953X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_QCA9556:
		ath79_soc = ATH79_SOC_QCA9556;
		chip = "9556";
		rev = id & QCA955X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_QCA9558:
		ath79_soc = ATH79_SOC_QCA9558;
		chip = "9558";
		rev = id & QCA955X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_QCA956X:
		ath79_soc = ATH79_SOC_QCA956X;
		chip = "956X";
		rev = id & QCA956X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_TP9343:
		ath79_soc = ATH79_SOC_TP9343;
		chip = "9343";
		rev = id & QCA956X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_QCN5502:
		ath79_soc = ATH79_SOC_QCN5502;
		chip = "5502";
		rev = id & QCN550X_REV_ID_REVISION_MASK;
		break;

	default:
		panic("ath79: unknown SoC, id:0x%08x", id);
	}

	if (ver == 1)
		ath79_soc_rev = rev;

	if (soc_is_qca953x() || soc_is_qca955x() || soc_is_qca956x() ||
		soc_is_qcn550x())
		snprintf(ath79_sys_type, sizeof(ath79_sys_type),
			"Qualcomm Atheros QCA%s ver %u rev %u", chip, ver, rev);
	else if (soc_is_tp9343())
		snprintf(ath79_sys_type, sizeof(ath79_sys_type),
			"Qualcomm Atheros TP%s rev %u", chip, rev);
	else
		snprintf(ath79_sys_type, sizeof(ath79_sys_type),
			"Atheros AR%s rev %u", chip, rev);
	pr_info("SoC: %s\n", ath79_sys_type);
}

const char *get_system_type(void)
{
	return ath79_sys_type;
}

int get_c0_perfcount_int(void)
{
	return ATH79_MISC_IRQ(5);
}
EXPORT_SYMBOL_GPL(get_c0_perfcount_int);

unsigned int get_c0_compare_int(void)
{
	return CP0_LEGACY_COMPARE_IRQ;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int ath79_be_handler(struct pt_regs *regs, int is_fixup)
{
#ifdef CONFIG_SOC_AR934X
	printk("ath data bus error: cause %#x epc %#lx\nrebooting...", read_c0_cause(), read_c0_epc());
	ath79_restart("bus_error");
#else
	printk("ath data bus error: cause %#x\n", read_c0_cause());
#endif
	return (is_fixup ? MIPS_BE_FIXUP : MIPS_BE_FATAL);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void __init plat_device_tree_setup(void) {

#if defined CONFIG_AVM_ENHANCED

    struct boot_param_header *dtb;

        char *rev_str;
        int rev = 0;
		char *subrev_str;
		int subrev = 0;

        rev_str = prom_getenv("HWRevision");
        if (rev_str) {
			if (sscanf(rev_str, "%u", &rev) != 1) {
				rev_str = NULL;
            } else {
                switch ( rev ) {
                case 190:
                    mips_machtype = ATH79_MACH_AVM_HW190;
                    break;
                case 194:
                    mips_machtype = ATH79_MACH_AVM_HW194;
                    break;
                case 200:
                    mips_machtype = ATH79_MACH_AVM_HW200;
                    break;
                case 201:
                    mips_machtype = ATH79_MACH_AVM_HW201;
                    break;
                case 205:
                    mips_machtype = ATH79_MACH_AVM_HW205;
                    break;
                case 206:
                    mips_machtype = ATH79_MACH_AVM_HW206;
                    break;
                case 214:
                    mips_machtype = ATH79_MACH_AVM_HW214;
                    break;
                case 215:
                    mips_machtype = ATH79_MACH_AVM_HW215;
                    break;
                case 216:
                    mips_machtype = ATH79_MACH_AVM_HW216;
                    break;
                case 219:
                    mips_machtype = ATH79_MACH_AVM_HW219;
                    break;
                case 222:
                    mips_machtype = ATH79_MACH_AVM_HW222;
                    break;
                case 238:
                    mips_machtype = ATH79_MACH_AVM_HW238;
                    break;
                case 240:
		case 241:
                    mips_machtype = ATH79_MACH_AVM_HW240;
                    break;
                default:
                    printk("%s: Unknown HWRevision %d for ATH79 Architecture\n", __func__, rev);
                    rev_str = NULL;
                    break;
                }
            }
		}
		if (!rev_str) {
			printk("%s: Unable to read AVM hardware "
				 "Revision! Identity crisis... who am I?\n",
				 __func__);
		}

		subrev_str = prom_getenv("HWSubRevision");
		if (subrev_str) {
			if (sscanf(subrev_str, "%u", &subrev) != 1)
				subrev_str = NULL;
		}
		if (!subrev_str) {
			printk("%s: Unable to read AVM hardware "
				 "subrevision! Identity crisis... who am I?\n",
				 __func__);
		}

		printk("%s: AVM hardware subrevision %d\n", __func__,
			 subrev);

		if (subrev > avm_subrev_max) {
			printk("%s: Too many hardware subrevisions!\n", __func__);
			panic("%s: Too many hardware subrevisions!\n", __func__);
		}

		dtb = (struct boot_param_header *)avm_kernel_config_device_tree[subrev];

		if (!dtb) {  /* fallback auf subrev == 0 */
		    dtb = (struct boot_param_header *)avm_kernel_config_device_tree[0];
			printk("%s: Fallback device-tree for AVM hardware "
				 "subrevision %d\n", __func__, subrev);
        }

		if (!dtb) {
			printk("%s: Missing device-tree for AVM hardware "
				 "subrevision %d\n", __func__, subrev);
			panic("%s: Missing device-tree for AVM hardware "
				 "subrevision %d\n", __func__, subrev);
		} else {
            extern void *initial_boot_params;
            initial_boot_params = (void *) dtb;
            printk("DT @ %p: %02x %02x %02x %02x %02x %02x %02x %02x\n",
                    dtb,
                    ((unsigned char *)dtb)[0],
                    ((unsigned char *)dtb)[1],
                    ((unsigned char *)dtb)[2],
                    ((unsigned char *)dtb)[3],
                    ((unsigned char *)dtb)[4],
                    ((unsigned char *)dtb)[5],
                    ((unsigned char *)dtb)[6],
                    ((unsigned char *)dtb)[7]);
        }

	__dt_setup_arch(dtb);

#endif
}
void __init plat_mem_setup(void)
{
    char *s, *p;
    unsigned int memsize = 0;
    phys_addr_t memstart = PHYS_OFFSET;

	board_be_handler = ath79_be_handler;
	_machine_restart = ath79_restart;
	_machine_halt = ath79_halt;
	pm_power_off = ath79_halt;
    printk("Set Port base\n");
	set_io_port_base(KSEG1);
    printk("detect_sys_type\n");


	ath79_reset_base = ioremap_nocache(AR71XX_RESET_BASE, AR71XX_RESET_SIZE);
	ath79_pll_base   = ioremap_nocache(AR71XX_PLL_BASE  , AR71XX_PLL_SIZE  );
	
    ath79_detect_sys_type();

    s = prom_getenv("memsize");
    if (s) {
        memsize = simple_strtoul(s, &p, 16);
    } else {
        printk("[%s] memsize konnte nicht aus env ermittelt werden\n", __func__);
        BUG_ON(1);
    }

    /* Use when SQFS is positioned directly behind the kernel-image */
    s = prom_getenv("memstart");
    if (s) {
        printk("[%s] found memstart in env\n", __func__);
        memstart = simple_strtoul(s, &p, 16);
    }

    printk("[%s] memsize 0x%x, memstart 0x%08x\n", __func__, memsize, memstart);

    add_memory_region(memstart, memsize, BOOT_MEM_RAM);
}

void __init plat_time_init(void)
{
	unsigned long cpu_clk_rate;
	unsigned long ahb_clk_rate;
	unsigned long ddr_clk_rate;
	unsigned long ref_clk_rate;

	ath79_clocks_init();

	cpu_clk_rate = ath79_get_sys_clk_rate("cpu");
	ahb_clk_rate = ath79_get_sys_clk_rate("ahb");
	ddr_clk_rate = ath79_get_sys_clk_rate("ddr");
	ref_clk_rate = ath79_get_sys_clk_rate("ref");

	pr_info("Clocks: CPU:%lu.%03luMHz, DDR:%lu.%03luMHz, AHB:%lu.%03luMHz, Ref:%lu.%03luMHz\n",
		cpu_clk_rate / 1000000, (cpu_clk_rate / 1000) % 1000,
		ddr_clk_rate / 1000000, (ddr_clk_rate / 1000) % 1000,
		ahb_clk_rate / 1000000, (ahb_clk_rate / 1000) % 1000,
		ref_clk_rate / 1000000, (ref_clk_rate / 1000) % 1000);

	mips_hpt_frequency = cpu_clk_rate / 2;
}

__setup("board=", mips_machtype_setup);

static int __init ath79_setup(void)
{
	of_platform_populate(NULL, of_default_bus_match_table, NULL, NULL);
	if (mips_machtype == ATH79_MACH_GENERIC_OF) {
#if defined(ATH79_WLAN_FW_DUMP)
		ath79_init_wlan_fw_dump_buffer();
#endif
		return 0;
	}

/* === Replaced by AVM Implementatio === */
#if 0
    ath79_gpio_init();
#endif
	ath79_register_uart();
	ath79_register_wdt();

	mips_machine_setup();

	return 0;
}

arch_initcall(ath79_setup);

void __init device_tree_init(void)
{
 	unsigned long base, size;

	if (!initial_boot_params)
		return;

	base = virt_to_phys(initial_boot_params);
	size = be32_to_cpu(of_get_flat_dt_size());

	/* Before we do anything, lets reserve the dt blob */
	reserve_bootmem(base, size, BOOTMEM_DEFAULT);

	unflatten_device_tree();
}

static void __init ath79_generic_init(void)
{
	/* Nothing to do */
}

MIPS_MACHINE(ATH79_MACH_GENERIC,
	     "Generic",
	     "Generic AR71XX/AR724X/AR913X based board",
	     ath79_generic_init);

MIPS_MACHINE(ATH79_MACH_GENERIC_OF,
	     "DTB",
	     "Generic AR71XX/AR724X/AR913X based board (DT)",
	     NULL);

/*
 *  Copyright (C) 2008-2010 Gabor Juhos <juhosg@openwrt.org>
 *  Copyright (C) 2008 Imre Kaloz <kaloz@openwrt.org>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */
#ifndef __ASM_MACH_ATH79_IRQ_H
#define __ASM_MACH_ATH79_IRQ_H

#define MIPS_CPU_IRQ_BASE	0
#define NR_IRQS			    91

#define ATH79_CPU_IRQ(_x)	(MIPS_CPU_IRQ_BASE + (_x))

#define ATH79_MISC_IRQ_BASE	    0x10
#define ATH79_MISC_IRQ_COUNT	32
#define ATH79_MISC_IRQ(_x)	    (ATH79_MISC_IRQ_BASE + (_x))

#define ATH79_GPIO_IRQ_BASE     (ATH79_MISC_IRQ_BASE + ATH79_MISC_IRQ_COUNT)
#define ATH79_GPIO_IRQ_COUNT	32
#define ATH79_GPIO_IRQ(_x)	    (ATH79_GPIO_IRQ_BASE + (_x))

#define ATH79_GPIO_IRQn(_gpio)  (ATH79_GPIO_IRQ_BASE+(_gpio))
#define ATH_GPIO_IRQn(_gpio)  	(ATH79_GPIO_IRQ_BASE+(_gpio))

#define ATH79_PCI_IRQ_BASE	    (ATH79_GPIO_IRQ_BASE + ATH79_GPIO_IRQ_COUNT)
#define ATH79_PCI_IRQ_COUNT	    6
#define ATH79_PCI_IRQ(_x)	    (ATH79_PCI_IRQ_BASE + (_x))

#define ATH79_IP2_IRQ_BASE	    (ATH79_PCI_IRQ_BASE + ATH79_PCI_IRQ_COUNT)
#define ATH79_IP2_IRQ_COUNT	    2
#define ATH79_IP2_IRQ(_x)	    (ATH79_IP2_IRQ_BASE + (_x))

#define ATH79_IP3_IRQ_BASE	    (ATH79_IP2_IRQ_BASE + ATH79_IP2_IRQ_COUNT)
#define ATH79_IP3_IRQ_COUNT     3
#define ATH79_IP3_IRQ(_x)       (ATH79_IP3_IRQ_BASE + (_x))

#define ATH_CPU_IRQ_WLAN		ATH79_CPU_IRQ(2)
#define ATH_CPU_IRQ_USB			ATH79_CPU_IRQ(3)
#define ATH_CPU_IRQ_GE0			ATH79_CPU_IRQ(4)
#define ATH_CPU_IRQ_GE1			ATH79_CPU_IRQ(5)
#define ATH_CPU_IRQ_MISC		ATH79_CPU_IRQ(6)
#define ATH_CPU_IRQ_TIMER		ATH79_CPU_IRQ(7)
#define ATH_CPU_IRQ_PCI			ATH79_CPU_IRQ(8)

#define ATH_CPU_IRQ_PCI_EP		ATH79_PCI_IRQ_BASE	// Either EP or RC

/*
 * Interrupts connected to the CPU->Misc line.
 */
#define MISC_BIT_TIMER			    0
#define MISC_BIT_ERROR			    1
#define MISC_BIT_GPIO			    2
#define MISC_BIT_UART			    3
#define MISC_BIT_WATCHDOG			4
#define MISC_BIT_PC         		5
#if defined(CONFIG_ATH_HS_UART) || defined(CONFIG_ATH_HS_UART_MODULE) || defined(CONFIG_SERIAL_AVM_ATH_HI) || defined(CONFIG_SERIAL_AVM_ATH_HI_MODULE)
#define MISC_BIT_HS_UART			6
#else                                                    
#define MISC_BIT_OHCI_USB			6
#endif                                                   
#define MISC_BIT_MBOX			    7
#define MISC_BIT_TIMER2    		    8
#define MISC_BIT_TIMER3    		    9
#define MISC_BIT_TIMER4    		    10
#define MISC_BIT_DDR_PERF           11
#define MISC_BIT_ENET_LINK			12
#define MISC_BIT_NAT_AGER			13
#define MISC_BIT_OTP                14
#define MISC_BIT_CHKSUM_ACC         15
#define MISC_BIT_DDR_SF_ENTRY       16
#define MISC_BIT_DDR_SF_EXIT        17
#define MISC_BIT_DDR_ACT_IN_SF      18
#define MISC_BIT_SLIC               19
#define MISC_BIT_WOW                20
#define MISC_BIT_NANDF              21
#define MISC_BIT_PGPIO              22
#define MISC_BIT_FDC                23
#define MISC_BIT_I2C                24
#define MISC_BIT_USB1_PLL_LOCK      25
#define MISC_BIT_USB2_PLL_LOCK      26
#define MISC_BIT_I2C                24

#define __misc_int(x)		(ATH79_MISC_IRQ_BASE + MISC_BIT_ ##x)

#define ATH_MISC_IRQ_TIMER		        __misc_int(TIMER)
#define ATH_MISC_IRQ_ERROR		        __misc_int(ERROR)
#define ATH_MISC_IRQ_GPIO		        __misc_int(GPIO)
#define ATH_MISC_IRQ_UART		        __misc_int(UART)
#define ATH_MISC_IRQ_WATCHDOG		    __misc_int(WATCHDOG)
#define ATH_MISC_IRQ_PERF_COUNTER	    __misc_int(PC)
#define ATH_MISC_IRQ_UART1		        __misc_int(UART1)
#define ATH_MISC_IRQ_HS_UART	        __misc_int(HS_UART)
#define ATH_MISC_IRQ_DMA		        __misc_int(MBOX)
#define ATH_MISC_IRQ_TIMER2		        __misc_int(TIMER2)
#define ATH_MISC_IRQ_TIMER3		        __misc_int(TIMER3)
#define ATH_MISC_IRQ_TIMER4		        __misc_int(TIMER4)
#define ATH_MISC_IRQ_DDR_PERF		    __misc_int(DDR_PERF)
#define ATH_MISC_IRQ_ENET_LINK		    __misc_int(ENET_LINK)
#define ATH_MISC_IRQ_NAT_AGER		    __misc_int(NAT_AGER)
#define ATH_MISC_IRQ_OTP		        __misc_int(OTP)
#define ATH_MISC_IRQ_CHKSUM_ACC		    __misc_int(CHKSUM_ACC)
#define ATH_MISC_IRQ_DDR_SF_ENTRY	    __misc_int(DDR_SF_ENTRY)
#define ATH_MISC_IRQ_DDR_SF_EXIT	    __misc_int(DDR_SF_EXIT)
#define ATH_MISC_IRQ_DDR_ACTIVITY_IN_SF __misc_int(DDR_ACTIVITY_IN_SF)
#define ATH_MISC_IRQ_SLIC		        __misc_int(SLIC_INTR)
#define ATH_MISC_IRQ_WOW		        __misc_int(WOW)
#define ATH_MISC_IRQ_NANDF		        __misc_int(NANDF)
#define ATH_MISC_IRQ_PGPIO		        __misc_int(PGPIO_INT)
#define ATH_MISC_IRQ_FDC		        __misc_int(FDC)
#define ATH_MISC_IRQ_I2C		        __misc_int(I2C)
#define ATH_MISC_IRQ_USB1_PLL_LOCK	    __misc_int(USB1_PLL_LOCK)
#define ATH_MISC_IRQ_USB2_PLL_LOCK	    __misc_int(USB2_PLL_LOCK)

#include_next <irq.h>

#endif /* __ASM_MACH_ATH79_IRQ_H */

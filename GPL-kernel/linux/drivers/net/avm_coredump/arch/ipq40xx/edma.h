/*
 * Copyright (c) 2014 - 2017, The Linux Foundation. All rights reserved.
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all copies.
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef _EDMA_H_
#define _EDMA_H_

#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/io.h>
#include <linux/vmalloc.h>
#include <linux/pagemap.h>
#include <linux/smp.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/sysctl.h>
#include <linux/phy.h>
#include <linux/of_net.h>
#include <net/checksum.h>
#include <net/ip6_checksum.h>
#include <asm-generic/bug.h>
#include <linux/version.h>
#include "ess_edma.h"

#define EDMA_CPU_CORES_SUPPORTED 4
#define EDMA_MAX_PORTID_SUPPORTED 5
#define EDMA_MAX_VLAN_SUPPORTED  EDMA_MAX_PORTID_SUPPORTED
#define EDMA_MAX_PORTID_BITMAP_INDEX (EDMA_MAX_PORTID_SUPPORTED + 1)
#define EDMA_MAX_PORTID_BITMAP_SUPPORTED 0x1f	/* 0001_1111 = 0x1f */
#define EDMA_MAX_NETDEV_PER_QUEUE 4 /* 3 Netdev per queue, 1 space for indexing */

#define EDMA_MAX_RECEIVE_QUEUE 8
#define EDMA_MAX_TRANSMIT_QUEUE 16

/* WAN/LAN adapter number */
#define EDMA_WAN 0
#define EDMA_LAN 1

/* VLAN tag */
#define EDMA_LAN_DEFAULT_VLAN 1
#define EDMA_WAN_DEFAULT_VLAN 2

#define EDMA_DEFAULT_GROUP1_VLAN 2
#define EDMA_DEFAULT_GROUP2_VLAN 1
#define EDMA_DEFAULT_GROUP3_VLAN 3
#define EDMA_DEFAULT_GROUP4_VLAN 4
#define EDMA_DEFAULT_GROUP5_VLAN 5

#define EDMA_DEFAULT_GROUP1_BMP 0x20
#define EDMA_DEFAULT_GROUP2_BMP 0x1e

#define EDMA_DEFAULT_DISABLE_RSS 0
#define EDMA_RSS_DISABLE 1
#define EDMA_RSS_ENABLE 0

/* Queues exposed to linux kernel */
#define EDMA_NETDEV_TX_QUEUE 4
#define EDMA_NETDEV_RX_QUEUE 4

/* Number of queues per core */
#define EDMA_NUM_TXQ_PER_CORE 4
#define EDMA_NUM_RXQ_PER_CORE 2

#define EDMA_TPD_EOP_SHIFT 31

#define EDMA_PORT_ID_SHIFT 12
#define EDMA_PORT_ID_MASK 0x7

/* tpd word 3 bit 18-28 */
#define EDMA_TPD_PORT_BITMAP_SHIFT 18

#define EDMA_TPD_FROM_CPU_SHIFT 25

#define EDMA_FROM_CPU_MASK 0x80
#define EDMA_SKB_PRIORITY_MASK 0x38

/* TX/RX descriptor ring count */
/* should be a power of 2 */
#define EDMA_RX_RING_SIZE 512
#define EDMA_TX_RING_SIZE 512

/* Flags used in paged/non paged mode */
#define EDMA_RX_HEAD_BUFF_SIZE_JUMBO 256
#define EDMA_RX_HEAD_BUFF_SIZE 1540

/* MAX frame size supported by switch */
#define EDMA_MAX_JUMBO_FRAME_SIZE 9216

/* Configurations */
#define EDMA_INTR_CLEAR_TYPE 0
#define EDMA_INTR_SW_IDX_W_TYPE 0
#define EDMA_FIFO_THRESH_TYPE 0
#define EDMA_RSS_TYPE 0
#define EDMA_RX_IMT 0x0020
#define EDMA_TX_IMT 0x0050
#define EDMA_TPD_BURST 5
#define EDMA_TXF_BURST 0x100
#define EDMA_RFD_BURST 8
#define EDMA_RFD_THR 16
#define EDMA_RFD_LTHR 0

/* RX/TX per CPU based mask/shift */
#define EDMA_TX_PER_CPU_MASK 0xF
#define EDMA_RX_PER_CPU_MASK 0x3
#define EDMA_TX_PER_CPU_MASK_SHIFT 0x2
#define EDMA_RX_PER_CPU_MASK_SHIFT 0x1
#define EDMA_TX_CPU_START_SHIFT 0x2
#define EDMA_RX_CPU_START_SHIFT 0x1

/* FLags used in transmit direction */
#define EDMA_HW_CHECKSUM 0x00000001
#define EDMA_VLAN_TX_TAG_INSERT_FLAG 0x00000002
#define EDMA_VLAN_TX_TAG_INSERT_DEFAULT_FLAG 0x00000004

#define EDMA_SW_DESC_FLAG_LAST 0x1
#define EDMA_SW_DESC_FLAG_SKB_HEAD 0x2
#define EDMA_SW_DESC_FLAG_SKB_FRAG 0x4
#define EDMA_SW_DESC_FLAG_SKB_FRAGLIST 0x8
#define EDMA_SW_DESC_FLAG_SKB_NONE 0x10
#define EDMA_SW_DESC_FLAG_SKB_REUSE 0x20


#define EDMA_MAX_SKB_FRAGS (MAX_SKB_FRAGS + 1)

/* Ethtool specific list of EDMA supported features */
#define EDMA_SUPPORTED_FEATURES (SUPPORTED_10baseT_Half \
					| SUPPORTED_10baseT_Full \
					| SUPPORTED_100baseT_Half \
					| SUPPORTED_100baseT_Full \
					| SUPPORTED_1000baseT_Full)

/* Recevie side atheros Header */
#define EDMA_RX_ATH_HDR_VERSION 0x2
#define EDMA_RX_ATH_HDR_VERSION_SHIFT 14
#define EDMA_RX_ATH_HDR_PRIORITY_SHIFT 11
#define EDMA_RX_ATH_PORT_TYPE_SHIFT 6
#define EDMA_RX_ATH_HDR_RSTP_PORT_TYPE 0x4

/* Transmit side atheros Header */
#define EDMA_TX_ATH_HDR_PORT_BITMAP_MASK 0x7F
#define EDMA_TX_ATH_HDR_FROM_CPU_MASK 0x80
#define EDMA_TX_ATH_HDR_FROM_CPU_SHIFT 7

#define EDMA_TXQ_START_CORE0 8
#define EDMA_TXQ_START_CORE1 12
#define EDMA_TXQ_START_CORE2 0
#define EDMA_TXQ_START_CORE3 4

#define EDMA_TXQ_IRQ_MASK_CORE0 0x0F00
#define EDMA_TXQ_IRQ_MASK_CORE1 0xF000
#define EDMA_TXQ_IRQ_MASK_CORE2 0x000F
#define EDMA_TXQ_IRQ_MASK_CORE3 0x00F0

#define EDMA_ETH_HDR_LEN 12
#define EDMA_ETH_TYPE_MASK 0xFFFF

#define EDMA_RX_BUFFER_WRITE 16
#define EDMA_RFD_AVAIL_THR 80

#define EDMA_GMAC_NO_MDIO_PHY	PHY_MAX_ADDR

#ifdef CONFIG_AVM_NET_EDMA
/* Ethtool specific list of GMAC supported features */
#define EDMA_GMAC_SUPPORTED_FEATURES	(SUPPORTED_10baseT_Half		\
					| SUPPORTED_10baseT_Full	\
					| SUPPORTED_100baseT_Half	\
					| SUPPORTED_100baseT_Full	\
					| SUPPORTED_1000baseT_Full	\
					| SUPPORTED_Autoneg		\
					| SUPPORTED_TP			\
					| SUPPORTED_Pause		\
					| SUPPORTED_Asym_Pause)

/* Ethtool specific list of GMAC advertised features */
#define EDMA_GMAC_ADVERTISED_FEATURES	(ADVERTISED_10baseT_Half	\
					| ADVERTISED_10baseT_Full	\
					| ADVERTISED_100baseT_Half	\
					| ADVERTISED_100baseT_Full	\
					| ADVERTISED_1000baseT_Full	\
					| ADVERTISED_Autoneg		\
					| ADVERTISED_TP			\
					| ADVERTISED_Pause		\
					| ADVERTISED_Asym_Pause)
#endif

/* edma transmit descriptor */
struct edma_tx_desc {
	__le16  len; /* full packet including CRC */
	__le16  svlan_tag; /* vlan tag */
	__le32  word1; /* byte 4-7 */
	__le32  addr; /* address of buffer */
	__le32  word3; /* byte 12 */
};

#endif /* _EDMA_H_ */

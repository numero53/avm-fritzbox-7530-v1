#!/usr/bin/perl -w
##########################################################################################
# vim: fileencoding=utf8
##########################################################################################

#### FIXME Only for test FIXME ####
#  export FRITZ_BOX_BUILD_DIR=/home/blabitzke/fbox/GU_RELEASE
#  export KERNEL_LAYOUT=iks
#### FIXME Only for test FIXME ####
use utf8;
use strict;
use diagnostics;    # For verbose warning messages
use Cwd qw(getcwd);
use Carp qw(cluck); # For debugging purposes
use POSIX qw(:termios_h);
use Term::ANSIColor qw(:constants);
$Term::ANSIColor::AUTORESET = 1;
use sigtrap qw(die normal-signals);

my $KERNEL_CLASS = $ARGV[0];
my $DRIVER_NAME = $ARGV[1];

#print "KERNEL_CLASS = '$KERNEL_CLASS'\n"; # FIXME
#print "DRIVER_NAME  = '$DRIVER_NAME'\n";  # FIXME

my @split_path = split('/', getcwd);
my $relative_source_dir = '../../' . join('/', @{split_path[-3 .. -1]});
system("ln -fs " . $relative_source_dir . "/include/linux_avmnet.h ../../../include/linux/avmnet.h");
system("ln -fs " . $relative_source_dir . "/include/linux_ifx_ppa_api_common.h ../../../include/net/ifx_ppa_api_common.h");
system("ln -fs " . $relative_source_dir . "/include/linux_ifx_ppa_api_directpath.h ../../../include/net/ifx_ppa_api_directpath.h");
system("ln -fs " . $relative_source_dir . "/include/linux_ifx_ppa_api.h ../../../include/net/ifx_ppa_api.h");
system("ln -fs " . $relative_source_dir . "/include/linux_ifx_ppa_hook.h ../../../include/net/ifx_ppa_hook.h");
system("ln -fs " . $relative_source_dir . "/include/linux_ifx_ppa_ppe_hal.h ../../../include/net/ifx_ppa_ppe_hal.h");
system("ln -fs " . $relative_source_dir . "/include/linux_ifx_ppa_stack_al.h ../../../include/net/ifx_ppa_stack_al.h");
system("ln -fs " . $relative_source_dir . "/include/linux_ltq_eth_oam_handler.h ../../../include/net/ltq_eth_oam_handler.h");

if($KERNEL_CLASS eq "") {
    $KERNEL_CLASS = 26;
}

unless(-e "./Makefile.$KERNEL_CLASS") {
    exit 0; # No architecture specific Makefile available
}
unlink("./Makefile");
system("ln -sf ./Makefile.$KERNEL_CLASS ./Makefile");


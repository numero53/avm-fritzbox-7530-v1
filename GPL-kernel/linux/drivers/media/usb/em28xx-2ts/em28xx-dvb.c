/*
 DVB device driver for em28xx

 (c) 2008 Mauro Carvalho Chehab <mchehab@infradead.org>

 (c) 2008 Devin Heitmueller <devin.heitmueller@gmail.com>
	- Fixes for the driver to properly work with HVR-950
	- Fixes for the driver to properly work with Pinnacle PCTV HD Pro Stick
	- Fixes for the driver to properly work with AMD ATI TV Wonder HD 600

 (c) 2008 Aidan Thornton <makosoft@googlemail.com>

 Based on cx88-dvb, saa7134-dvb and videobuf-dvb originally written by:
	(c) 2004, 2005 Chris Pascoe <c.pascoe@itee.uq.edu.au>
	(c) 2004 Gerd Knorr <kraxel@bytesex.org> [SuSE Labs]

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License.
 */

#include <linux/kernel.h>
#include <linux/usb.h>

#include "em28xx.h"
#include <media/v4l2-common.h>
#include <media/videobuf-vmalloc.h>
#include <media/tuner.h>

#ifndef EM28XX_AVM_ONLY
#include "tuner-simple.h"

#include "lgdt330x.h"
#include "zl10353.h"
#include "s5h1409.h"
#include "mt352.h"
#include "mt352_priv.h"
#include "tda1002x.h"
#include "tda18271.h"
#endif

#include "dvb_frontend.h"
#include "dvb_demux.h"
#include "dmxdev.h"
#include "dvb_net.h"
#include "tda18271c2dd.h"
#include "drxk.h"
#include "mxl251.h"

#include <linux/avm_led_event.h>


MODULE_DESCRIPTION("driver for em28xx based DVB cards");
MODULE_AUTHOR("Mauro Carvalho Chehab <mchehab@infradead.org>");
MODULE_LICENSE("GPL");

static unsigned int debug;
module_param(debug, int, 0644);
MODULE_PARM_DESC(debug, "enable debug messages [dvb]");

DVB_DEFINE_MOD_OPT_ADAPTER_NR(adapter_nr);

#define dprintk(level, fmt, arg...) do {			\
if (debug >= level) 						\
	printk(KERN_DEBUG "%s/2-dvb: " fmt, dev->name, ## arg);	\
} while (0)


static inline void print_err_status(struct em28xx *dev,
				     int packet, int status)
{
	char *errmsg = "Unknown";

	switch (status) {
	case -ENOENT:
		errmsg = "unlinked synchronuously";
		break;
	case -ECONNRESET:
		errmsg = "unlinked asynchronuously";
		break;
	case -ENOSR:
		errmsg = "Buffer error (overrun)";
		break;
	case -EPIPE:
		errmsg = "Stalled (device not responding)";
		break;
	case -EOVERFLOW:
		errmsg = "Babble (bad cable?)";
		break;
	case -EPROTO:
		errmsg = "Bit-stuff error (bad cable?)";
		break;
	case -EILSEQ:
		errmsg = "CRC/Timeout (could be anything)";
		break;
	case -ETIME:
		errmsg = "Device does not respond";
		break;
	}
	if (packet < 0) {
		dprintk(1, "URB status %d [%s].\n", status, errmsg);
	} else {
		dprintk(1, "URB packet %d, status %d [%s].\n",
			packet, status, errmsg);
	}
}

#ifdef EM28XX_CC_DROP_CHECK
int drop_check(struct em28xx_dvb *dvb, unsigned char *buf, int length) {
 
	int i, nframes = length / 188;
	unsigned pid, cc;
	
	for (i = 0; i < nframes; i++) {

		pid = ((buf[(i*188)+1] & 0x1f) << 8) + buf[(i*188)+2];
		cc = buf[(i*188)+3] & 0x0f;

		if (pid >= EM28XX_MAX_PID) {
			continue;
			//printk(KERN_INFO "PID TOO BIG pid %d (sizeof %d) %x %x cc %d!!\n", pid, sizeof(pid),
			//buf[(i*188)+1] & 0x1f, buf[(i*188)+2], cc);
		}
		
		if ((dvb->cc_check[pid].last_cc != -1) && (cc != (dvb->cc_check[pid].last_cc +1) % 16)) {
			dvb->cc_check[pid].error_counter++;
		}

		dvb->cc_check[pid].last_cc = cc;
	}
	return nframes;
}
#endif

static inline int em28xx_dvb_urb_data_copy(struct em28xx *dev, struct urb *urb, int index)
{
	int xfer_bulk, num_packets, i;

	if (!dev)
		return 0;

	if (dev->disconnected)
		return 0;

	if (urb->status < 0)
		print_err_status(dev, -1, urb->status);

	xfer_bulk = usb_pipebulk(urb->pipe);

	if (xfer_bulk) /* bulk */
		num_packets = 1;
	else /* isoc */
		num_packets = urb->number_of_packets;

	for (i = 0; i < num_packets; i++) {

		if (xfer_bulk) {
			if (urb->status < 0) {
				print_err_status(dev, i, urb->status);
				if (urb->status != -EPROTO)
				continue;
			}

#ifdef EM28XX_CC_DROP_CHECK
			drop_check(dev->dvb[index], urb->transfer_buffer, urb->actual_length);
#endif
			dvb_dmx_swfilter(&dev->dvb[index]->demux, urb->transfer_buffer,
					urb->actual_length);
		} else {

			if (urb->iso_frame_desc[i].status < 0) {
				print_err_status(dev, i,
						 urb->iso_frame_desc[i].status);
				if (urb->iso_frame_desc[i].status != -EPROTO)
					continue;
			}

#ifdef EM28XX_CC_DROP_CHECK
			drop_check(dev->dvb[index], urb->transfer_buffer + urb->iso_frame_desc[i].offset,
				 urb->iso_frame_desc[i].actual_length);
#endif
			
			if (urb->iso_frame_desc[i].actual_length > 0) {

				dvb_dmx_swfilter(&dev->dvb[index]->demux,
						 urb->transfer_buffer +
					 urb->iso_frame_desc[i].offset,
					 urb->iso_frame_desc[i].actual_length);
			}
		}
	}

	return 0;
}

static int em28xx_start_streaming(struct em28xx_dvb *dvb)
{
	int rc;
	struct em28xx *dev = dvb->adapter.priv;
	int dvb_max_packet_size, packet_multiplier, dvb_alt, usb_dvb_num_bufs;

	if (dev->dvb_xfer_bulk) {
		if (!dev->dvb_ep_bulk[0])
			return -ENODEV;
		dvb_max_packet_size = 512; /* USB 2.0 spec */
		packet_multiplier = EM28XX_DVB_BULK_PACKET_MULTIPLIER;
		dvb_alt = 0;
		usb_dvb_num_bufs = EM28XX_DVB_NUM_BUFS_BULK;
	} else { /* isoc */
		if (!dev->dvb_ep_isoc[0])
			return -ENODEV;
		dvb_max_packet_size = dev->dvb_max_pkt_size_isoc;
		if (dvb_max_packet_size < 0)
			return dvb_max_packet_size;
		packet_multiplier = EM28XX_DVB_NUM_ISOC_PACKETS;
		dvb_alt = dev->dvb_alt_isoc;
		usb_dvb_num_bufs = EM28XX_DVB_NUM_BUFS_ISOC;
	}

	//Only set interface if necessary
	if (dev->dvb_alt_intf_in_use != dvb_alt) {
		usb_set_interface(dev->udev, 0, dvb_alt);
		dev->dvb_alt_intf_in_use = dvb_alt;
	}

	rc = em28xx_set_mode(dev, EM28XX_DIGITAL_MODE);
	if (rc < 0)
		return rc;

/*	printk(KERN_INFO "%s: Using %d buffers each with %d x %d bytes\n",
		__FUNCTION__,
		usb_dvb_num_bufs,
		packet_multiplier,
		dvb_max_packet_size);*/

	return em28xx_init_usb_xfer(dev, EM28XX_DIGITAL_MODE,
				    dev->dvb_xfer_bulk,
				    usb_dvb_num_bufs,
				    dvb_max_packet_size,
				    packet_multiplier,
				    dvb->index,
				    em28xx_dvb_urb_data_copy);
}

static int em28xx_stop_streaming(struct em28xx_dvb *dvb)
{
	struct em28xx *dev = dvb->adapter.priv;

	em28xx_stop_urbs(dev, EM28XX_DIGITAL_MODE, dvb->index);

	return 0;
}

#ifdef EM28XX_PID_FILTERING
static u16 pid_checker_low_byte[16] = {0xE0, 0xE2, 0xE4, 0xE6, 0xE8, 0xEA, 0xEC, 0xEE,
					0xF0, 0xF2, 0xF4, 0xF6, 0xF8, 0xFA, 0xFC, 0xFE };
static u16 pid_checker_high_byte[16] = {0xE1, 0xE3, 0xE5, 0xE7, 0xE9, 0xEB, 0xED, 0xEF,
					0xF1, 0xF3, 0xF5, 0xF7, 0xF9, 0xFB, 0xFD, 0xFF };

static int em28xx_pid_filter_ctrl(struct em28xx_dvb *dvb,  int index, u16 pid, int onoff)
{
	struct em28xx *dev = dvb->adapter.priv;
	int ret = 0;

	if (dvb->filter_exceeded == 1) {
		return 0;
	}

	/* PID 0x2000 means no filtering */
	if (pid == 0x2000) {
		onoff = 0;
	}
		
	/* We can handle only 16 PIDs */
	if (index > 15) {
		dvb->filter_exceeded = 1;
		onoff = 0;
	}

	if (onoff == dvb->filter_on) {
		return 0;
	}

	if (onoff)
		ret = em28xx_write_reg_bits(dev, EM2874_R5F_TS_ENABLE, EM2874_TS1_FILTER_ENABLE, EM2874_TS1_FILTER_ENABLE );
	else
		ret = em28xx_write_reg_bits(dev, EM2874_R5F_TS_ENABLE, 0x00, EM2874_TS1_FILTER_ENABLE);

	dvb->filter_on = onoff;

	//printk(KERN_INFO "%s ret %d onoff %d\n", __FUNCTION__, ret,  onoff);

	return ret;
}

static int em28xx_pid_filter(struct em28xx_dvb *dvb, int index, u16 pid, int onoff)
{
	struct em28xx *dev = dvb->adapter.priv;
	int ret = 0;

	if (dvb->filter_exceeded == 1) {			
		return 0;
	}

	if (index > 15) {
		return -1;
	}

	ret = em28xx_write_reg(dev, pid_checker_low_byte[index], pid & 0xff);

	ret = em28xx_write_reg(dev, pid_checker_high_byte[index], pid >> 8);

	if (onoff)
		ret = em28xx_write_reg_bits(dev, pid_checker_high_byte[index], 0x80, 0x80);
	else
		ret = em28xx_write_reg_bits(dev, pid_checker_high_byte[index], 0x00, 0x80);

	//printk(KERN_INFO "%s index %d pid %d onoff: %d ret %d\n", __FUNCTION__, index, pid, onoff, ret);

	return ret;
}
#endif

static int em28xx_start_feed(struct dvb_demux_feed *feed)
{
	struct dvb_demux *demux  = feed->demux;
	struct em28xx_dvb *dvb = demux->priv;
	struct em28xx *dev = dvb->adapter.priv;
	int rc, ret;

	if (!demux->dmx.frontend)
		return -EINVAL;

	mutex_lock(&dev->lock);
	dvb->nfeeds++;
	rc = dvb->nfeeds;

#ifdef EM28XX_PID_FILTERING
	if (dvb->index == 0) {
		/* enable HW PID filter for TS 1*/
		ret = em28xx_pid_filter_ctrl(dvb, feed->index, feed->pid, 1);
		if (ret < 0)
			printk(KERN_INFO "%s: pid_filter_ctrl(, 1) failed=%d\n", KBUILD_MODNAME, ret);
	}
#endif

	/* ask device to start streaming */
	if (dvb->nfeeds == 1) {
#ifdef EM28XX_CC_DROP_CHECK
		{
			int i;
			for(i = 0; i < EM28XX_MAX_PID; i++) {
				dvb->cc_check[i].last_cc = -1;
				dvb->cc_check[i].error_counter = 0;
			}
		}
#endif
		ret = em28xx_start_streaming(dvb);
		if (ret < 0)
			rc = ret;
	}

#ifdef EM28XX_PID_FILTERING
	if (dvb->index == 0) {
		/* add PID to device HW PID filter */
		ret = em28xx_pid_filter(dvb, feed->index, feed->pid, 1);
		if (ret < 0)
			printk(KERN_INFO "%s: em28xx_pid_filter(%d, %d) failed=%d\n", KBUILD_MODNAME, feed->index, feed->pid, ret);
	}
#endif

	mutex_unlock(&dev->lock);
	return rc;
}

static int em28xx_stop_feed(struct dvb_demux_feed *feed)
{
	struct dvb_demux *demux  = feed->demux;
	struct em28xx_dvb *dvb = demux->priv;
	struct em28xx *dev = dvb->adapter.priv;
	int err = 0;

	mutex_lock(&dev->lock);

#ifdef EM28XX_PID_FILTERING
	if (dvb->index == 0) {
		/* add PID to device HW PID filter */
		err = em28xx_pid_filter(dvb, feed->index, feed->pid, 0);
		if (err < 0)
			printk(KERN_INFO "%s: em28xx_pid_filter(%d, %d) failed=%d\n", KBUILD_MODNAME, feed->index, feed->pid, err);
	}
#endif
	dvb->nfeeds--;

	if (0 == dvb->nfeeds)
		err = em28xx_stop_streaming(dvb);

	mutex_unlock(&dev->lock);
	return err;
}


/* ------------------------------------------------------------------ */
static int em28xx_dvb_bus_ctrl(struct dvb_frontend *fe, int acquire)
{
	struct em28xx *dev = fe->dvb->priv;

	if (acquire)
		return em28xx_set_mode(dev, EM28XX_DIGITAL_MODE);
	else
		return em28xx_set_mode(dev, EM28XX_SUSPEND);
}

/* ------------------------------------------------------------------ */

#ifndef EM28XX_AVM_ONLY
static struct lgdt330x_config em2880_lgdt3303_dev = {
	.demod_address = 0x0e,
	.demod_chip = LGDT3303,
};

static struct zl10353_config em28xx_zl10353_with_xc3028 = {
	.demod_address = (0x1e >> 1),
	.no_tuner = 1,
	.parallel_ts = 1,
	.if2 = 45600,
};

static struct s5h1409_config em28xx_s5h1409_with_xc3028 = {
	.demod_address = 0x32 >> 1,
	.output_mode   = S5H1409_PARALLEL_OUTPUT,
	.gpio          = S5H1409_GPIO_OFF,
	.inversion     = S5H1409_INVERSION_OFF,
	.status_mode   = S5H1409_DEMODLOCKING,
	.mpeg_timing   = S5H1409_MPEGTIMING_CONTINOUS_NONINVERTING_CLOCK
};

static struct zl10353_config em28xx_zl10353_xc3028_no_i2c_gate = {
	.demod_address = (0x1e >> 1),
	.no_tuner = 1,
	.disable_i2c_gate_ctrl = 1,
	.parallel_ts = 1,
	.if2 = 45600,
};
#endif

#ifdef EM28XX_DRX397XD_SUPPORT
/* [TODO] djh - not sure yet what the device config needs to contain */
static struct drx397xD_config em28xx_drx397xD_with_xc3028 = {
	.demod_address = (0xe0 >> 1),
};
#endif

static struct drxk_config maxmedia_ub425_tc_drxk = {
	.adr = 0x29,
	.single_master = 1,
	.no_i2c_bridge = 1,
	//todo: member existiert nicht mehr, welche Folgen hat das ?
	//.load_firmware_sync = true,
};

#ifndef EM28XX_AVM_ONLY

static int mt352_terratec_xs_init(struct dvb_frontend *fe)
{
	/* Values extracted from a USB trace of the Terratec Windows driver */
	static u8 clock_config[]   = { CLOCK_CTL,  0x38, 0x2c };
	static u8 reset[]          = { RESET,      0x80 };
	static u8 adc_ctl_1_cfg[]  = { ADC_CTL_1,  0x40 };
	static u8 agc_cfg[]        = { AGC_TARGET, 0x28, 0xa0 };
	static u8 input_freq_cfg[] = { INPUT_FREQ_1, 0x31, 0xb8 };
	static u8 rs_err_cfg[]     = { RS_ERR_PER_1, 0x00, 0x4d };
	static u8 capt_range_cfg[] = { CAPT_RANGE, 0x32 };
	static u8 trl_nom_cfg[]    = { TRL_NOMINAL_RATE_1, 0x64, 0x00 };
	static u8 tps_given_cfg[]  = { TPS_GIVEN_1, 0x40, 0x80, 0x50 };
	static u8 tuner_go[]       = { TUNER_GO, 0x01};

	mt352_write(fe, clock_config,   sizeof(clock_config));
	udelay(200);
	mt352_write(fe, reset,          sizeof(reset));
	mt352_write(fe, adc_ctl_1_cfg,  sizeof(adc_ctl_1_cfg));
	mt352_write(fe, agc_cfg,        sizeof(agc_cfg));
	mt352_write(fe, input_freq_cfg, sizeof(input_freq_cfg));
	mt352_write(fe, rs_err_cfg,     sizeof(rs_err_cfg));
	mt352_write(fe, capt_range_cfg, sizeof(capt_range_cfg));
	mt352_write(fe, trl_nom_cfg,    sizeof(trl_nom_cfg));
	mt352_write(fe, tps_given_cfg,  sizeof(tps_given_cfg));
	mt352_write(fe, tuner_go,       sizeof(tuner_go));
	return 0;
}

static struct mt352_config terratec_xs_mt352_cfg = {
	.demod_address = (0x1e >> 1),
	.no_tuner = 1,
	.if2 = 45600,
	.demod_init = mt352_terratec_xs_init,
};

static struct tda10023_config em28xx_tda10023_config = {
	.demod_address = 0x0c,
	.invert = 1,
};

/* ------------------------------------------------------------------ */

static int attach_xc3028(u8 addr, struct em28xx *dev)
{
	struct dvb_frontend *fe;
	struct xc2028_config cfg;

	memset(&cfg, 0, sizeof(cfg));
	cfg.i2c_adap  = &dev->i2c_adap;
	cfg.i2c_addr  = addr;

	if (!dev->dvb->frontend) {
		printk(KERN_ERR "%s/2: dvb frontend not attached. "
				"Can't attach xc3028\n",
		       dev->name);
		return -EINVAL;
	}

	fe = dvb_attach(xc2028_attach, dev->dvb->frontend, &cfg);
	if (!fe) {
		printk(KERN_ERR "%s/2: xc3028 attach failed\n",
		       dev->name);
		dvb_frontend_detach(dev->dvb->frontend);
		dev->dvb->frontend = NULL;
		return -EINVAL;
	}

	printk(KERN_INFO "%s/2: xc3028 attached\n", dev->name);

	return 0;
}
#endif

/* ------------------------------------------------------------------ */

static int register_dvb(struct em28xx_dvb *dvb,
		 struct module *module,
		 struct em28xx *dev,
		 struct device *device)
{
	int result;

	mutex_init(&dvb->lock);

	/* register adapter */
	result = dvb_register_adapter(&dvb->adapter, dev->name, module, device,
				      adapter_nr);
	if (result < 0) {
		printk(KERN_WARNING "%s: dvb_register_adapter failed (errno = %d)\n",
		       dev->name, result);
		goto fail_adapter;
	}

	/* Ensure all frontends negotiate bus access */
	dvb->frontend->ops.ts_bus_ctrl = em28xx_dvb_bus_ctrl;

	dvb->adapter.priv = dev;

	/* register frontend */
	result = dvb_register_frontend(&dvb->adapter, dvb->frontend);
	if (result < 0) {
		printk(KERN_WARNING "%s: dvb_register_frontend failed (errno = %d)\n",
		       dev->name, result);
		goto fail_frontend;
	}

	/* register demux stuff */
	dvb->demux.dmx.capabilities =
		DMX_TS_FILTERING | DMX_SECTION_FILTERING |
		DMX_MEMORY_BASED_FILTERING;
	dvb->demux.priv       = dvb;
	dvb->demux.filternum  = 256;
	dvb->demux.feednum    = 256;
	dvb->demux.start_feed = em28xx_start_feed;
	dvb->demux.stop_feed  = em28xx_stop_feed;

	result = dvb_dmx_init(&dvb->demux);
	if (result < 0) {
		printk(KERN_WARNING "%s: dvb_dmx_init failed (errno = %d)\n",
		       dev->name, result);
		goto fail_dmx;
	}

	dvb->dmxdev.filternum    = 256;
	dvb->dmxdev.demux        = &dvb->demux.dmx;
	dvb->dmxdev.capabilities = 0;
	result = dvb_dmxdev_init(&dvb->dmxdev, &dvb->adapter);
	if (result < 0) {
		printk(KERN_WARNING "%s: dvb_dmxdev_init failed (errno = %d)\n",
		       dev->name, result);
		goto fail_dmxdev;
	}

	dvb->fe_hw.source = DMX_FRONTEND_0;
	result = dvb->demux.dmx.add_frontend(&dvb->demux.dmx, &dvb->fe_hw);
	if (result < 0) {
		printk(KERN_WARNING "%s: add_frontend failed (DMX_FRONTEND_0, errno = %d)\n",
		       dev->name, result);
		goto fail_fe_hw;
	}

	dvb->fe_mem.source = DMX_MEMORY_FE;
	result = dvb->demux.dmx.add_frontend(&dvb->demux.dmx, &dvb->fe_mem);
	if (result < 0) {
		printk(KERN_WARNING "%s: add_frontend failed (DMX_MEMORY_FE, errno = %d)\n",
		       dev->name, result);
		goto fail_fe_mem;
	}

	result = dvb->demux.dmx.connect_frontend(&dvb->demux.dmx, &dvb->fe_hw);
	if (result < 0) {
		printk(KERN_WARNING "%s: connect_frontend failed (errno = %d)\n",
		       dev->name, result);
		goto fail_fe_conn;
	}

	/* register network adapter */
	dvb_net_init(&dvb->adapter, &dvb->net, &dvb->demux.dmx);
	return 0;

fail_fe_conn:
	dvb->demux.dmx.remove_frontend(&dvb->demux.dmx, &dvb->fe_mem);
fail_fe_mem:
	dvb->demux.dmx.remove_frontend(&dvb->demux.dmx, &dvb->fe_hw);
fail_fe_hw:
	dvb_dmxdev_release(&dvb->dmxdev);
fail_dmxdev:
	dvb_dmx_release(&dvb->demux);
fail_dmx:
	dvb_unregister_frontend(dvb->frontend);
fail_frontend:
	dvb_frontend_detach(dvb->frontend);
	dvb_unregister_adapter(&dvb->adapter);
fail_adapter:
	return result;
}

static void em28xx_unregister_dvb(struct em28xx_dvb *dvb)
{
	dvb_net_release(&dvb->net);
	dvb->demux.dmx.remove_frontend(&dvb->demux.dmx, &dvb->fe_mem);
	dvb->demux.dmx.remove_frontend(&dvb->demux.dmx, &dvb->fe_hw);
	dvb_dmxdev_release(&dvb->dmxdev);
	dvb_dmx_release(&dvb->demux);
	dvb_unregister_frontend(dvb->frontend);
	dvb_frontend_detach(dvb->frontend);
	dvb_unregister_adapter(&dvb->adapter);
}

int usb_action_func(unsigned int led_id, enum _led_action action, led_callback_t callback_func)
{
	switch(action) {
		case led_ext_enable: printk(KERN_INFO "[%s] enable external LED %d\n", __FUNCTION__, led_id); break; //ignore??
		case led_ext_disable: printk(KERN_INFO "[%s] disable external LED %d\n", __FUNCTION__, led_id); break; //ignore??
		case led_ext_on: printk(KERN_INFO "[%s] setting external LED %d to on\n", __FUNCTION__, led_id); break;
		case led_ext_off: printk(KERN_INFO "[%s] setting external LED %d to off\n", __FUNCTION__, led_id); break;
		default: printk(KERN_INFO "[%s] setting external LED %d to off\n", __FUNCTION__, led_id); break;
	}

	return 0;
}


static int em28xx_dvb_init(struct em28xx *dev)
{
	int result = 0, i;
	struct em28xx_dvb *dvb;

	if (!dev->board.has_dvb) {
		/* This device does not support the extension */
		return 0;
	}

	for(i = 0; i < dev->n_endpoints; i++) {

	dvb = kzalloc(sizeof(struct em28xx_dvb), GFP_KERNEL);

	if (dvb == NULL) {
		printk(KERN_INFO "em28xx_dvb: memory allocation failed\n");
		return -ENOMEM;
	}
	dvb->index = i;
	dvb->parent = dev;
	dev->dvb[i] = dvb;

#ifdef EM28XX_PID_FILTERING
	dev->dvb[i]->filter_on = 0;
	dev->dvb[i]->filter_exceeded = 0;
#endif

#ifdef EM28XX_CC_DROP_CHECK
	{
		int i;
		for(i = 0; i < EM28XX_MAX_PID; i++) {
			dvb->cc_check[i].last_cc = -1;
			dvb->cc_check[i].error_counter = 0;
		}
	}
#endif

	em28xx_set_mode(dev, EM28XX_DIGITAL_MODE);
	/* init frontend */
	switch (dev->model) {
#ifndef EM28XX_AVM_ONLY
	case EM2883_BOARD_HAUPPAUGE_WINTV_HVR_850:
	case EM2883_BOARD_HAUPPAUGE_WINTV_HVR_950:
	case EM2880_BOARD_PINNACLE_PCTV_HD_PRO:
	case EM2880_BOARD_AMD_ATI_TV_WONDER_HD_600:
		dvb->frontend = dvb_attach(lgdt330x_attach,
					   &em2880_lgdt3303_dev,
					   &dev->i2c_adap);
		if (attach_xc3028(0x61, dev) < 0) {
			result = -EINVAL;
			goto out_free;
		}
		break;
	case EM2880_BOARD_KWORLD_DVB_310U:
		dvb->frontend = dvb_attach(zl10353_attach,
					   &em28xx_zl10353_with_xc3028,
					   &dev->i2c_adap);
		if (attach_xc3028(0x61, dev) < 0) {
			result = -EINVAL;
			goto out_free;
		}
		break;
	case EM2880_BOARD_HAUPPAUGE_WINTV_HVR_900:
	case EM2880_BOARD_EMPIRE_DUAL_TV:
		dvb->frontend = dvb_attach(zl10353_attach,
					   &em28xx_zl10353_xc3028_no_i2c_gate,
					   &dev->i2c_adap);
		if (attach_xc3028(0x61, dev) < 0) {
			result = -EINVAL;
			goto out_free;
		}
		break;
	case EM2880_BOARD_TERRATEC_HYBRID_XS:
	case EM2881_BOARD_PINNACLE_HYBRID_PRO:
		dvb->frontend = dvb_attach(zl10353_attach,
					   &em28xx_zl10353_xc3028_no_i2c_gate,
					   &dev->i2c_adap);
		if (dvb->frontend == NULL) {
			/* This board could have either a zl10353 or a mt352.
			   If the chip id isn't for zl10353, try mt352 */
			dvb->frontend = dvb_attach(mt352_attach,
						   &terratec_xs_mt352_cfg,
						   &dev->i2c_adap);
		}

		if (attach_xc3028(0x61, dev) < 0) {
			result = -EINVAL;
			goto out_free;
		}
		break;
	case EM2883_BOARD_KWORLD_HYBRID_330U:
	case EM2882_BOARD_EVGA_INDTUBE:
		dvb->frontend = dvb_attach(s5h1409_attach,
					   &em28xx_s5h1409_with_xc3028,
					   &dev->i2c_adap);
		if (attach_xc3028(0x61, dev) < 0) {
			result = -EINVAL;
			goto out_free;
		}
		break;
	case EM2882_BOARD_KWORLD_ATSC_315U:
		dvb->frontend = dvb_attach(lgdt330x_attach,
					   &em2880_lgdt3303_dev,
					   &dev->i2c_adap);
		if (dvb->frontend != NULL) {
			if (!dvb_attach(simple_tuner_attach, dvb->frontend,
				&dev->i2c_adap, 0x61, TUNER_THOMSON_DTT761X)) {
				result = -EINVAL;
				goto out_free;
			}
		}
		break;
	case EM2880_BOARD_HAUPPAUGE_WINTV_HVR_900_R2:
#ifdef EM28XX_DRX397XD_SUPPORT
		/* We don't have the config structure properly populated, so
		   this is commented out for now */
		dvb->frontend = dvb_attach(drx397xD_attach,
					   &em28xx_drx397xD_with_xc3028,
					   &dev->i2c_adap);
		if (attach_xc3028(0x61, dev) < 0) {
			result = -EINVAL;
			goto out_free;
		}
		break;
#endif
	case EM2870_BOARD_REDDO_DVB_C_USB_BOX:
		/* Philips CU1216L NIM (Philips TDA10023 + Infineon TUA6034) */
		dvb->frontend = dvb_attach(tda10023_attach,
			&em28xx_tda10023_config,
			&dev->i2c_adap, 0x48);
		if (dvb->frontend) {
			if (!dvb_attach(simple_tuner_attach, dvb->frontend,
				&dev->i2c_adap, 0x60, TUNER_PHILIPS_CU1216L)) {
				result = -EINVAL;
				goto out_free;
			}
		}
		break;
	case EM2874_BOARD_DELOCK_61959:
	case EM2874_BOARD_MAXMEDIA_UB425_TC:

		/* attach demodulator */
		dvb->frontend = dvb_attach(drxk_attach, &maxmedia_ub425_tc_drxk,
				&(dev->i2c_adap[0]));

		if (dvb->frontend) {

			/* disable I2C-gate */
			dvb->frontend->ops.i2c_gate_ctrl = NULL;

			/* attach tuner */
			if (!dvb_attach(tda18271c2dd_attach, dvb->frontend,
					&(dev->i2c_adap[0]), 0x60)) {
				dvb_frontend_detach(dvb->frontend);
				result = -EINVAL;
				goto out_free;
			}

		}

		/* TODO: we need drx-3913k firmware in order to support DVB-T */
		em28xx_info("MaxMedia UB425-TC/Delock 61959: only DVB-C " \
				"supported by that driver version\n");

		break;
#endif
	case EM28174_BOARD_FRITZ_REPEATER_450_DVB:

#if 1
		/* attach demodulator */
		dvb->frontend = dvb_attach(mxl251_attach, NULL,
				&(dev->i2c_adap[0]), 0x50);
		if (dvb->frontend == NULL) {
			result = -EINVAL;
			goto out_free;
		}
#endif
		/* disable I2C-gate */
		dvb->frontend->ops.i2c_gate_ctrl = NULL;

		/* attach tuner */
#if 0
		if (!dvb_attach(mxl251_attach, dvb->frontend,
				&(dev->i2c_adap[0]), 0x60)) {
			dvb_frontend_detach(dvb->frontend);
			result = -EINVAL;
			goto out_free;
		}
#endif

		//led_register_external(led_usb_leds, usb_action_func);

		em28xx_info("FRITZ!WLAN Repeater DVB\n");

		break;
	default:
		printk(KERN_ERR "%s/2: The frontend of your DVB/ATSC card"
				" isn't supported yet\n",
		       dev->name);
		break;
	}
	if (NULL == dvb->frontend) {
		printk(KERN_ERR
		       "%s/2: frontend initialization failed\n",
		       dev->name);
		result = -EINVAL;
		goto out_free;
	}
	/* define general-purpose callback pointer */
	dvb->frontend->callback = em28xx_tuner_callback;

	/* register everything */
	result = register_dvb(dvb, THIS_MODULE, dev, &dev->udev->dev);

	if (result < 0)
		goto out_free;


	}

	em28xx_set_mode(dev, EM28XX_SUSPEND);
	printk(KERN_INFO "Successfully loaded em28xx-dvb\n");
	return 0;

out_free:
	em28xx_set_mode(dev, EM28XX_SUSPEND);
	kfree(dvb);
	dev->dvb[i] = NULL;
	return result;
}

static inline void prevent_sleep(struct dvb_frontend_ops *ops)
{
	ops->set_voltage = NULL;
	ops->sleep = NULL;
	ops->tuner_ops.sleep = NULL;
}

static int em28xx_dvb_fini(struct em28xx *dev)
{
	int i;

	if (!dev->board.has_dvb) {
		/* This device does not support the extension */
		return 0;
	}

	for(i = 0; i < dev->n_endpoints; i++) {

	if (dev->dvb[i]) {
		struct em28xx_dvb *dvb = dev->dvb[i];

		if (dev->disconnected) {
			/* We cannot tell the device to sleep
			 * once it has been unplugged. */
#if 0
			if (dvb->fe[0])
				prevent_sleep(&dvb->fe[0]->ops);
			if (dvb->fe[1])
				prevent_sleep(&dvb->fe[1]->ops);
#endif
		}

		//if (dev->model == EM28174_BOARD_FRITZ_REPEATER_450_DVB)
		//	led_release_external(led_usb_leds);

		em28xx_unregister_dvb(dvb);
		kfree(dvb);
		dev->dvb[i] = NULL;
	}

	}

	return 0;
}

static struct em28xx_ops dvb_ops = {
	.id   = EM28XX_DVB,
	.name = "Em28xx dvb Extension",
	.init = em28xx_dvb_init,
	.fini = em28xx_dvb_fini,
};

static int __init em28xx_dvb_register(void)
{
	return em28xx_register_extension(&dvb_ops);
}

static void __exit em28xx_dvb_unregister(void)
{
	em28xx_unregister_extension(&dvb_ops);
}

module_init(em28xx_dvb_register);
module_exit(em28xx_dvb_unregister);

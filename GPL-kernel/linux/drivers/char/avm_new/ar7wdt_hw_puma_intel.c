/*------------------------------------------------------------------------------------------*\
 *
 * ar7wdt_hw_puma_intel.c
 * Description:
 *
 * an ar7 wrapper for the puma7 atom hw watchdog (iTCO_wdt)
 *   
 *   Copyright (C) 20018 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#if defined(CONFIG_AVM_WATCHDOG)

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/watchdog.h>
#include <linux/ar7wdt.h>



#define PUMA7_WATCHDOG_DEBUG 

#if defined(PUMA7_WATCHDOG_DEBUG)
#define DBG(...)  printk(__VA_ARGS__)
#else /*--- #if defined(PUMA7_WATCHDOG_DEBUG) ---*/
#define DBG(...)  
#endif /*--- #else ---*/ /*--- #if defined(PUMA7_WATCHDOG_DEBUG) ---*/

#if defined CONFIG_ITCO_WDT
	#error Sammeltreiber und linux-source passen nicht zusammen. Bitte linux-source mit ausgeschalteten watchdog wählen
#endif
extern int iTCO_wdt_init_module(void);
extern int _iTCO_wdt_ping(void);
extern int iTCO_wdt_stop(struct watchdog_device *wd_dev);
extern int iTCO_wdt_running(void);
extern void iTCO_wdt_cleanup_module(void);

void ar7wdt_hw_init(void)
{
    	iTCO_wdt_init_module();
	//wdt will be started immediately in iTCO's probe function
}



int ar7wdt_hw_is_wdt_running(void)
{
	return iTCO_wdt_running();
}



void ar7wdt_hw_secure_wdt_disable(void)
{
	iTCO_wdt_stop(NULL);
}



void ar7wdt_hw_deinit(void)
{

	iTCO_wdt_cleanup_module();
}



void ar7wdt_hw_reboot(void) {

    panic("[ar7wdt_hw_reboot] soft reboot\n");
}



void ar7wdt_hw_trigger(void)
{

	DBG("AR7 WatchDog Timer Driver ping !\n");
    	if(!ar7wdt_hw_is_wdt_running()) {
		DBG("waiting for iCTO watchdog ...\n");
		return;
    	}
	_iTCO_wdt_ping();
}
#endif


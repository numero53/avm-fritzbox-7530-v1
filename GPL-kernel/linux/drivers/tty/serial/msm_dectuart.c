/**--------------------------------------------------------------------------------**\
 * hooks for in msm_serial.c or msm_hsl_serial_lite.c
\**--------------------------------------------------------------------------------**/
#include <linux/errno.h>

#include "msm_dectuart.h"

struct _msm_dectuart msm_dectuart;
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
int msm_dectuart_get_char(void){
    struct _msm_dectuart *pdectuart = &msm_dectuart;

    if(pdectuart->msm_dectuart_get_char) {
        return pdectuart->msm_dectuart_get_char();
    }
    return -ENODEV;
}
EXPORT_SYMBOL(msm_dectuart_get_char);
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
void msm_dectuart_put_char(unsigned char ch){
    struct _msm_dectuart *pdectuart = &msm_dectuart;

    if(pdectuart->msm_dectuart_put_char) {
        pdectuart->msm_dectuart_put_char(ch);
    }
}
EXPORT_SYMBOL(msm_dectuart_put_char);
/**--------------------------------------------------------------------------------**\
 * mode: != 0  und Tx/Rx-Irq an
\**--------------------------------------------------------------------------------**/
void msm_dectuart_init(unsigned int baud, int mode){
    struct _msm_dectuart *pdectuart = &msm_dectuart;

    if(pdectuart->msm_dectuart_init) {
        pdectuart->msm_dectuart_init(baud, mode);
    }
}
EXPORT_SYMBOL(msm_dectuart_init);
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
void msm_console_stop(void) {
    struct _msm_dectuart *pdectuart = &msm_dectuart;

    if(pdectuart->msm_console_stop) {
        pdectuart->msm_console_stop();
    }
}
EXPORT_SYMBOL(msm_console_stop);
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
void msm_console_start(void) {
    struct _msm_dectuart *pdectuart = &msm_dectuart;

    if(pdectuart->msm_console_start) {
        pdectuart->msm_console_start();
    }
}
EXPORT_SYMBOL(msm_console_start);
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
void msm_dectuart_exit(void){
    struct _msm_dectuart *pdectuart = &msm_dectuart;

    if(pdectuart->msm_dectuart_exit) {
        pdectuart->msm_dectuart_exit();
    }
}
EXPORT_SYMBOL(msm_dectuart_exit);

#ifndef _AVM_HW_CONFIG_H_
#define _AVM_HW_CONFIG_H_

#define AVM_HW_CONFIG_VERSION 1

#include <linux/string.h>

#include <linux/avm_hw_config_def.h>

enum _avm_hw_param {
    avm_hw_param_no_param                   = AVM_DEF_HW_PARAM_NO_PARAM,

    avm_hw_param_gpio_out_active_low        = AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_LOW,
    avm_hw_param_gpio_out_active_high       = AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH,
    avm_hw_param_gpio_in_active_low         = AVM_DEF_HW_PARAM_GPIO_IN_ACTIVE_LOW,
    avm_hw_param_gpio_in_active_high        = AVM_DEF_HW_PARAM_GPIO_IN_ACTIVE_HIGH,
#if defined(CONFIG_ATH79)
    avm_hw_param_gpio_in_out_active_low     = AVM_DEF_HW_PARAM_GPIO_IN_OUT_ACTIVE_LOW,
    avm_hw_param_gpio_in_out_active_high    = AVM_DEF_HW_PARAM_GPIO_IN_OUT_ACTIVE_HIGH,
#endif

    avm_hw_param_last_param
};

enum _avm_hw_function { 

    avm_hw_gpio = AVM_DEF_HW_FUNCTION_GPIO_PIN,
    avm_hw_pinmux1  = AVM_DEF_HW_FUNCTION_PINMUX1,
    avm_hw_pinmux2  = AVM_DEF_HW_FUNCTION_PINMUX2,
    avm_hw_pinmux3  = AVM_DEF_HW_FUNCTION_PINMUX3,
    avm_hw_pinmux4  = AVM_DEF_HW_FUNCTION_PINMUX4,
    avm_hw_pinmux5  = AVM_DEF_HW_FUNCTION_PINMUX5,
    avm_hw_pinmux6  = AVM_DEF_HW_FUNCTION_PINMUX6,
    avm_hw_pinmux7  = AVM_DEF_HW_FUNCTION_PINMUX7,
    avm_hw_pinmux8  = AVM_DEF_HW_FUNCTION_PINMUX8,
    avm_hw_pinmux9  = AVM_DEF_HW_FUNCTION_PINMUX9,
    avm_hw_pinmux10 = AVM_DEF_HW_FUNCTION_PINMUX10,
    avm_hw_pinmux11 = AVM_DEF_HW_FUNCTION_PINMUX11,
    avm_hw_pinmux12 = AVM_DEF_HW_FUNCTION_PINMUX12,
    avm_hw_pinmux13 = AVM_DEF_HW_FUNCTION_PINMUX13,
    avm_hw_pinmux14 = AVM_DEF_HW_FUNCTION_PINMUX14,
    avm_hw_pinmux15 = AVM_DEF_HW_FUNCTION_PINMUX15,
    avm_hw_nochange = AVM_DEF_HW_FUNCTION_PIN_NOCHANGE
};

#if defined(CONFIG_ATH79)
enum _avm_hw_default_value {
    AVM_HW_DEFAULT_VALUE_HIGH   = 1,
    AVM_HW_DEFAULT_VALUE_LOW    = 0,
    AVM_HW_DEFAULT_VALUE_UNDEF  = -1,
};
#define AVM_DEF_HW_FUNCTION_GPIO (AVM_DEF_HW_FUNCTION_PIN_NOCHANGE + 1)
#endif

struct _avm_hw_config {
    char *name;
    int value;
    enum _avm_hw_param param;
    enum _avm_hw_function function;
#if defined(CONFIG_ATH79)
    int func;
    enum _avm_hw_default_value default_value;
#endif
};

struct _avm_hw_config_table {
    unsigned int hwrev;
    unsigned int hwsubrev;
    struct _avm_hw_config *table;
};

extern struct _avm_hw_config *avm_current_hw_config;

int init_gpio_config(void);

struct _avm_hw_config *avm_get_hw_config_table(void);

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static inline struct _avm_hw_config *_avm_get_hw_configstruct(const char *name) {
    unsigned i;
    if(!avm_current_hw_config) {
        avm_current_hw_config = avm_get_hw_config_table();
        if(!avm_current_hw_config)
            return NULL;
    }
    for(i = 0; avm_current_hw_config[i].name; i++) {
        if(strcmp(avm_current_hw_config[i].name, name) == 0) {
            return &avm_current_hw_config[i];
        }
    }
    return NULL;
}
/*------------------------------------------------------------------------------------------*\
 * Config-Wert name nicht gefunden:
 *  return -1, p_value & p_param nicht gültig
 * version != AVM_HW_CONFIG_VERSION:
 *  return -2, p_value & p_param nicht gültig
 * sonst:
 *  return 0
\*------------------------------------------------------------------------------------------*/
static inline int avm_get_hw_config(unsigned int version, const char *name, int *p_value, enum _avm_hw_param *p_param) {
    struct _avm_hw_config *phwconfig;
    if(version != AVM_HW_CONFIG_VERSION) {
        return -2;
    }
    phwconfig = _avm_get_hw_configstruct(name);
    if(phwconfig == NULL) {
        return -1;
    }
    if (p_value) *p_value = phwconfig->value;
    if(p_param)  *p_param = phwconfig->param;
    return 0;
}

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static inline int avm_get_hw_config_function(unsigned int version, const char *name, enum _avm_hw_function *p_func) {
    struct _avm_hw_config *phwconfig;
    if(version != AVM_HW_CONFIG_VERSION) {
        return -2;
    }
    phwconfig = _avm_get_hw_configstruct(name);
    if(phwconfig == NULL) {
        return -1;
    }
    if (p_func) *p_func = phwconfig->function;
    return 0;
}
#endif /*--- #ifndef _AVM_HW_CONFIG_H_ ---*/


#ifndef _INCLUDE_LINUX_AVM_KERNEL_CONFIG_H_
#define _INCLUDE_LINUX_AVM_KERNEL_CONFIG_H_

#include <linux/err.h>
#include <linux/avm_kernel_config_device_tree.h>

//Only used as fallback, if location is not stored in device-tree under reserved-memory with name 'avm_reboot_string'
#define AVM_REBOOT_STRING_LOCATION  (0xa0001000) 
#define AVM_REBOOT_STRING_SIZE      512


struct _avm_kernel_module_memory_config {
	char *name;
	unsigned int core_size;
	unsigned int symbol_size;		/*---- CONFIG_KALLSYMS_ALL set ---*/
	unsigned int symbol_text_size;		/*---- CONFIG_KALLSYMS_ALL not set (only text-symbols) ---*/	
};

struct _avm_kernel_config {
	enum _avm_kernel_config_tags tag;
	void *config;
};

struct _avm_kernel_version_info {
	char buildnumber[32];
	char svnversion[32];
	char firmwarestring[128];
};

struct _avm_kernel_urlader_env {
	char name[64];
	char value[256];
};

extern struct _avm_kernel_config **avm_kernel_config;
extern struct _avm_kernel_module_memory_config *avm_kernel_module_memory_config;
extern unsigned char *avm_kernel_config_device_tree[avm_subrev_max];
extern struct _avm_kernel_version_info *avm_kernel_version_info;
extern struct _avm_kernel_urlader_env *avm_kernel_urlader_env;

static inline int init_avm_kernel_config_ptr(void)
{
	extern unsigned int __avm_kernel_config_start __attribute__ ((weak));

	if (IS_ERR(&__avm_kernel_config_start) ||
		(&__avm_kernel_config_start == NULL))
		return -1;

	avm_kernel_config =
		(struct _avm_kernel_config **)&__avm_kernel_config_start;

	return 0;
}

extern void init_avm_kernel_config(void);

#endif
/* vim: set noexpandtab sw=8 ts=8 sts=0: */
